import random
import traceback
import accountslib
import checkoutlib
import merch
import product
import validation as val
import jibber
from page import *


#Main checkout tests to be run for every checkout change
def master_checkout(self, site, login, paytype, loyalty, card):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)

    failures = 0
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "30000", 'beta')
        pointsBefore = sql.getLoyaltyPoints(email, 'beta')
        productPage.getProductPage(site, '319', 'beta')
        #misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'harry677', login)
        pointsUsed = paymentOptsPage.loyaltyInput(loyalty)
        paymentOptsPage.selectPaytype(paytype)
        addressPage.checkoutAddress(site, login, paytype)
        braintreePage.useBraintree(site, paytype, loyalty, card, cvv='150')
        orderNo = confirmPage.returnOrderNo(site)
        pointsAfter = sql.getLoyaltyPoints(email, 'beta')
        failures = misc.checkPointsDeduction(pointsBefore, pointsAfter, pointsUsed, failures)
        misc.logOut(wd, site, login)
        set_order_status(orderNo, 'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'no', orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

def mobile_master_checkout(self, site, login, paytype, loyalty, card):
    wd = self.wd
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    try:
        misc.wait(site)
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        pointsBefore = sql.getLoyaltyPoints(email, 'beta')
        wd.delete_all_cookies()
        productPage.getProductPage(site, '319', 'beta')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        misc.wait(site)
        productPage.addProductToBasketMobile()
        misc.wait(site)
        interstitialPage.proceedToBasketMobile()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', login)
        misc.wait(site)
        pointsUsed = paymentOptsPage.loyaltyInput(loyalty)
        paymentOptsPage.selectPaytype(paytype)
        misc.wait(site)
        addressPage.checkoutAddress(site, login, paytype)
        braintreePage.useBraintree(site, paytype, loyalty, card, cvv='150')
        orderNo = confirmPage.returnOrderNo(site)
        pointsAfter = sql.getLoyaltyPoints(email, 'beta')
        failures = misc.checkPointsDeduction(pointsBefore, pointsAfter, pointsUsed, failures)
        set_order_status(orderNo, 'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'no', orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures


def container_checkout(self, site, login, paytype, loyalty, card, container):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    print ('-----------------------------')
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print("Doing a test using the multi step checkout")
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        productPage.getProductPageContainer(site, '319', container)
        #Getting rid of GDPR cookie banner
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', login)
        pointsUsed = paymentOptsPage.loyaltyInput(loyalty)
        paymentOptsPage.selectPaytype(paytype)
        addressPage.checkoutAddress(site, login, paytype)
        braintreePage.useBraintree(site, paytype, loyalty, card, cvv='150')
        orderNo = confirmPage.returnOrderNo(site)
        confirmPage.getHomeContainer(site, container)
        confirmPage.logOut(site, login)
        set_order_status(orderNo, 'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'no', orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

def checkout_large(self, targetContainerID, site, productIDs, login, paytype, loyalty, card):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    ''' Set the variable with the container number if running tests in Containers
    otherwise leave it set to 'beta' 
    -----------------------------'''
    containerNumber = 'beta'
    '''--------------------------'''
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ("Testing large order...")
    try:
        email = misc.getEmailCheckout(site)
        misc.add_multiple_items_to_basket(site, targetContainerID, productPage, productIDs)

        interstitialPage.proceedToBasketOPC(site)
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutOPC(site, login, 'NON', paytype, card, '150', email, confirmAddress=0)
        orderNo = confirm.returnOrderNoOPC(site)
        failures = jibber.checkOrderStatus(orderNo, 'Approved', failures)

        # In Jibber checkthe number of cols in the order matches the number of items added to the basket
        set_order_status(orderNo,'Suspended')

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print('fell over')
    print('-' * 20)
    return failures

def simple_opc_checkout(self, targetContainerID, site, productID, login, paytype, loyalty, card, confirmAddress):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    ''' Set the variable with the container number if running tests in Containers
    otherwise leave it set to 'beta' 
    -----------------------------'''
    containerNumber = 'beta'
    '''--------------------------'''
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ("Doing a test using the OPC checkout")
    try:
        email = misc.getEmailCheckout(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        misc.checkAndUpdateStock(site, productID)
        productPage.getSelectedProductPage(site, productID, targetContainerID)
        productPage.clearCountrySwitchBanner()
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)

        pc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, confirmAddress)

        time.sleep(5)

        if containerNumber != 'beta' and 'KL' in paytype:
            print('Re-directing to container {} /checkout/confirm/'.format(containerNumber))
            time.sleep(5) # This sleep is needed to allow the the localhost page to load before re-directing
            opc.get('https://lovehoneycouk-{}.container.ci/checkout/confirm/'.format(containerNumber))

        orderNo = confirm.returnOrderNoOPC(site)

        orderNo = orderNo.strip()  # Needed for Safari to strip out a leading white space

        print("No Jibber Check")
        print('Order Confirmation order No: ' + orderNo)

        confirm.clickHomePageLink()

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

def opc_checkout(self, targetContainerID, site, productID, login, paytype, loyalty, card, confirmAddress, checkOpcEmail):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    win = Winnow(self.wd)
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    pointsUsed = '0'
    ''' Set the variable with the container number if running tests in Containers
    otherwise leave it set to 'beta' 
    -----------------------------'''
    containerNumber = 'beta'
    '''--------------------------'''
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ("Doing a test using the OPC checkout")
    try:
        email = misc.getEmailCheckout(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        pointsBefore = sql.getLoyaltyPoints(email, 'beta')
        misc.checkAndUpdateStock(site, productID)
        productPage.getSelectedProductPage(site, productID, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)

        if checkOpcEmail == 1:  # Change the email address in the OPC
            # # Get the email from the top of the OPC form
            # OPCEmail = opc.getOPCEmail()

            changedEmail = opc.changeOPCEmail('changedemail@lovehoney.co.uk')
            failures = opc.checkEmailChanged('changedemail@lovehoney.co.uk', changedEmail, failures)
            email = changedEmail

        if checkOpcEmail == 2:  # Add a new email then cancel the change
            opc.cancelEmailChange()

        pointsUsed = opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, confirmAddress)

        if containerNumber != 'beta' and 'KL' in paytype:
            print('Re-directing to container {} /checkout/confirm/'.format(containerNumber))
            time.sleep(5) # This sleep is needed to allow the the localhost page to load before re-directing
            opc.get('https://lovehoneycouk-{}.container.ci/checkout/confirm/'.format(containerNumber))

        # Testing that the Order Confirmation email matches the OPC email now that it is possible for users to modify it
        orderConfEmail = confirm.returnOrderEmail()

        if checkOpcEmail > 0:
            failures = confirm.checkOrderConfEmail(email, orderConfEmail, failures)

        orderNo = confirm.returnOrderNoOPC(site)
        grandTotal = confirm.returnGrandTotal()

        orderNo = orderNo.strip()  # Needed for Safari to strip out a leading white space

        failures = confirm.checkUSSalesTax(site, login, paytype, loyalty, grandTotal, failures)

        # confirm.clickHomePageLink()
        if (loyalty == 'ALL' or loyalty == 'HAL'):
            pointsAfter = sql.getLoyaltyPoints(email, 'beta')
            print ('Before: {} Used: {} After:{}'.format(pointsBefore, pointsUsed, pointsAfter ))
            failures = misc.checkPointsDeduction(pointsBefore, pointsAfter, pointsUsed, failures)
        else:
            print('Loyalty = NA or NON, no need to check points')
        set_order_status(orderNo,'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'yes',orderNo)
        if failures > 0:
            print("FAIL")
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures


def klarna_checkout_declined(self, targetContainerID, eyes, site, productID, login, paytype, loyalty, testName, declinedEmail, card, pDA, width, height):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    win = Winnow(self.wd)
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    pointsUsed = '0'
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ("Doing a test using the OPC checkout")
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=testName, viewport_size={'width': width, 'height': height})

        if declinedEmail == 'NA':
            email = misc.getEmailCheckout(site)
        else:
            email = declinedEmail

        sql.add_loyalty_points(email, "20000", 'beta')
        pointsBefore = sql.getLoyaltyPoints(email, 'beta')
        misc.checkAndUpdateStock(site, productID)
        productPage.getSelectedProductPage(site, productID, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)

        opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, confirmAddress='YES')

        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Klara declined message - email')
        opc.acceptKlarnaDeclinedMessage()
        misc.eyes_check_window_fluid(eyes, site, 'OPC, post decline')

        if pDA == 'card':
            print('PDA = Card payment')
            opc.selectCardPayment()
            opc.opcEnterCardDetails('UK','4012000033330026','150')
            misc.eyes_check_window_fluid(eyes, site, 'Card payment confirmation')
        elif pDA == 'KL-30':
            print('PDA = Pay in 30')
            self.clickElementByXpath("//input[@for='paymentMethodKlarna']")
            time.sleep(5)  # Wait for the Karna content to load before continuing
            self.click_gdpr()
            self.waitForId(self.submitButton)
            self.clickElementById(self.submitButton)
            misc.eyes_check_window_fluid(eyes, site, 'Pay in 30 confirmation')

        print('Finished')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures


def opc_checkout_mobile(self,  targetContainerID, site, login, paytype, loyalty, card):
    wd = self.wd
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    pointsUsed = '0'
    print ('-' * 20)
    print ("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ("Doing a test using the OPC checkout")
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        pointsBefore = sql.getLoyaltyPoints(email, 'beta')
        if(site == 'ES'):
            productPage.getSelectedProductPage(site, '9670',  targetContainerID)
        else:
            productPage.getSelectedProductPage(site, '319',  targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasketMobile()
        interstitialPage.proceedToBasketMobile()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        self.wd.execute_script("window.scrollTo(0, 300);")  # Scroll down the page on Mobile
        pointsUsed = opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, 'na')
        orderNo = confirm.returnOrderNoOPC(site)
        confirm.clickHomePageLink()
        if(loyalty !=  'NA'):
            pointsAfter = sql.getLoyaltyPoints(email, 'beta')
            print ('{2} {0} {1}'.format(pointsUsed, pointsAfter, pointsBefore))
            failures = misc.checkPointsDeduction(pointsBefore, pointsAfter, pointsUsed, failures)
        else:
            print('Loyalty = NA, no need to check points')
        set_order_status(orderNo,'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'yes',orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

def opc_container_checkout(self, site, login, paytype, loyalty, card, confirmAddress, container):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    print('-' * 20)
    print("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ('-----------------------------')
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        product.getProductPageContainer(wd, site, '38263', container)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        if site == 'CM':
            wd.refresh()    # Refresh the page to fix the payment buttons intermittently not loading
            print('Cart page refresh for CM')
        basketPage.selectCountry(site)
        time.sleep(3)
        basketPage.proceedToOPCContainer(site, paytype, container)
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, confirmAddress)
        orderNo = confirm.returnOrderNoOPC(site)
        confirm.getHomeContainer(site, container)
        confirm.logOut(site, login)
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'yes', orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

def opc_checkout_afterpay(self, targetContainerID, site, login, paytype, loyalty, card):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    print('-' * 20)
    print("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ('-----------------------------')
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        misc.checkAndUpdateStock(site, '319')
        productPage.getSelectedProductPage(site, '319', targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, 'na')
        orderNo = confirm.returnOrderNoOPC(site)
        misc.logOut(wd, site, login)
        set_order_status(orderNo,'Suspended')
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'yes',orderNo)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

def opc_checkout_secpay(self, site, login, paytype, card, eyes):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    print('-' * 20)
    print("Site: {0}  Login: {1}  Paytype: {2}".format(site, login, paytype))
    print ('-----------------------------')
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='SecPay Checkout {} {}'.format(site, paytype))

        email = misc.getEmail(site)
        misc.checkAndUpdateStock(site, '319')
        # productPage.getProductPage(site, '319', 'beta')   # Use this if you want to run the test on Beta
        productPage.getProductPageContainer(site, '319', '2314')
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        if paytype == 'CC-SEC' or paytype == 'AF':
            # Take a screen shot with the red 'required' fields displayed
            misc.eyes_check_window_fluid(eyes, site, 'Required fields - Login {} Paytype {}'.format(login, paytype))
        opc.runCheckoutOPC(site, login, 'NA', paytype, card, '150', email, 'na')
        # Take a screenshot with all fields filled and the Checkout button set to green
        time.sleep(5)  #  Sleep is here to allow the AfterPay screen to close
        misc.eyes_check_window_fluid(eyes, site, ' Login {} Paytype {}'.format(login, paytype))
        misc.logOut(wd, site, login)
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def shipping_address_checkout_bug(self, eyes, site, failures):

    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    print('-' * 20)
    print("Testing Checkout shipping address bug...")
    print("------------------------")
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='Shipping copied to Billing')

        email = misc.getEmail(site)
        misc.checkAndUpdateStock(site, '319')
        productPage.getProductPage(site, '319', 'beta')
        # productPage.getProductPageContainer(site, '319', '2391')
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', 'LOT', 'CC')

        opc.opcEnterDeliveryAddressCheckConfirm('First Line', 'Bath', 'BaNES','BA1 3EN', 'United Kingdom', 'YES')
        opc.clickToChangeDeliveryAddress()

        misc.eyes_check_window_fluid(eyes, 'UK ', 'Should be NO address in Billing')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def billing_address_clear_bug(self, eyes, site, failures):

    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    print('-' * 20)
    print("Testing Checkout shipping address bug...")
    print("------------------------")
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='Billing is cleared')

        email = misc.getEmail(site)
        misc.checkAndUpdateStock(site, '319')
        productPage.getProductPage(site, '319', 'beta')
        # productPage.getProductPageContainer(site, '319', '2391')
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', 'LOT', 'CC')

        opc.opcEnterDeliveryAddressCheckConfirm('First Line', 'Bath', 'BaNES','BA1 3EN', 'United Kingdom', 'NO')
        opc.opcEnterBillingAddressCheckConfirm('First Line Billing', 'Bath', 'BaNES','BA1 3EN', 'United Kingdom', 'NO')

        opc.saveDeliveryAddress()
        time.sleep(3)

        misc.eyes_check_window_fluid(eyes, 'UK ', 'Billing Address should not be cleared')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def opc_checkout_us_sales_tax(self, targetContainerID, eyes, site, paytype, loginState, address, city, state, zipCode, confirmAddress, invalidAddress):
    wd = self.wd
    win = Winnow(self.wd)
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    print('-' * 20)
    print("State: {}  ZipCode: {}".format(state, zipCode))
    print("------------------------")
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='{} | {} | {} | {}'.format(loginState, paytype, state, zipCode))

        # win.flickUSSalesTaxSwitch('.beta')

        email = misc.getEmail(site)
        misc.checkAndUpdateStock(site, '319')
        productPage.getSelectedProductPage(site, '319', targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        wd.refresh()  # Refresh the page to fix the payment buttons intermittently not loading
        print('Cart page refresh for CM')
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', loginState, paytype)
        if paytype == 'PP':
            opc.paypalPaymentOPC(site)
            try:
                self.sendTextById(self.shippingPhoneNo, '3423452334')
            except:
                print('No phone number required')
        elif paytype == 'AP':
            opc.amazonpayPaymentOPCWithoutSubmit('LOT', 'auto.testerComAZ@lovehoney.co.uk')
        opc.showOrderSummaryDetails()

        if paytype == 'CC' and loginState == 'LOT': # No need for this step with PP and AZP as they have a pre-filled address at this point so Sales Tax will be displayed
            misc.eyes_check_window_fluid(eyes, site, ' {}-{} Checkout - no tax showing'.format(loginState, paytype))
            print('Confirm Address: ' + confirmAddress)
            opc.opcEnterDeliveryAddressCheckConfirm(address, city, state, zipCode, 'United States', confirmAddress)
        else:
            opc.enterDeliveryPhoneNumOnly()

        if paytype == 'CC':
            opc.opcLoyalty(loginState, 'NON')

        opc.changeShippingOption('shippingOption1')

        confirmStr = ' '
        if confirmAddress == 'NO':
            confirmStr = ' NoConf, '

        # time.sleep(100)

        if invalidAddress == 'NO':
            misc.eyes_check_window_fluid(eyes, site, ' {}-{} Checkout,{}tax now showing'.format(loginState, paytype, confirmStr))
            # Change shipping -  tax may change as some states tax the shipping cost as well
            opc.changeShippingOption('shippingOption3')
            misc.eyes_check_window_fluid(eyes, site, ' {}-{} Checkout,{}shipping changed'.format(loginState, paytype, confirmStr))
        else:
            misc.eyes_check_window_fluid(eyes, site, ' Invalid Address - No tax showing')

        if paytype == 'CC':
            opc.opcEnterCardDetails(site, '4000000000000002', '150')
        else:
            opc.clickGdprAndSubmitOnly()
        time.sleep(5)

        if invalidAddress == 'NO':
            misc.eyes_check_window_fluid(eyes, site, ' {}-{} Order Confirmation'.format(loginState, paytype))
        else:
            misc.eyes_check_window_fluid(eyes, site, ' Invalid Address - No tax in Order Summary')

        misc.logOut(wd, site, 'LOT')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def pdp_check(self, targetContainerID, eyes, site, productId):
    wd = self.wd
    win = Winnow(self.wd)
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)

    print('-' * 20)
    print("PDP check for product {}".format(productId))
    print("------------------------")
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools',
                  test_name='Product {} PDP'.format(productId))

        misc.checkAndUpdateStock(site, productId)
        productPage.getSelectedProductPage(site, productId, targetContainerID)
        productPage.accept_gdpr_cookies()
        misc.eyes_check_window_fluid(eyes, site, ' PDP')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print('-' * 20)
    return failures

def opc_checkout_afterpay_declined(self, targetContainerID, site, login, paytype, loyalty, card):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    print('-' * 20)
    print("Site: {0}  Login: {1}  Paytype: {2}  Loyalty: {3}".format(site, login, paytype, loyalty))
    print ('-----------------------------')
    try:
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        if targetContainerID == 'beta':
            product.getProductPage(wd, site, '319', targetContainerID )
        else:
            product.getProductPageContainer(wd, site, '319', targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        try:
            basketPage.selectCountry(site)
        except:
            print('AUS already selected')
        basketPage.proceedToOPC(paytype)
        misc.check_country_screen(wd)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutOPC(site, login, loyalty, paytype, card, '150', email, 'na')
        failures = basketPage.checkAfterpayDeclined(failures)
        misc.logOut(wd, site, login)
        #jibber.logOut(site, login)
        # jibber.removeCookies()

    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

#Checkout tests using GVs in different combinations
#Amount: Any number between 10.00 and 500.00 (with decimal places)
def gv_checkout(self, site, login, paytype, amount):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitalPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}  Login: {1}  Paytype: {2}'.format(site, login, paytype))
    try:
        email = misc.getEmail(site)
        sql.addRandom_GV(site, amount, 'beta')
        sql.add_loyalty_points(email, "20000", 'beta')
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        GV_code1 = jibber.getGVcode1()
        GV_code2 = jibber.getGVcode2(paytype)
        productPage.getProductPage(site, '319', 'beta')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitalPage.proceedToBasket()
        basketPage.changeBasketQuantity('7')
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', login)
        paymentOptsPage.loyaltyInputGv(paytype, login)
        paymentOptsPage.enterGvInfo(paytype, GV_code1, GV_code2)
        paymentOptsPage.selectPaytype(paytype)
        addressPage.checkoutAddress(site, login, paytype)
        braintreePage.useBraintreeGv(site, paytype, '4012000033330026', '150')
        orderNo = confirmPage.returnOrderNo(site)
        misc.logOut(wd, site, login)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.enterOrderNo(orderNo)
        failures = jibber.checkPaymentsGv(paytype, failures)
    except:
        failures = failures+1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

def gv_opc_checkout(self, targetContainerID, site, login, loyalty, paytype, amount):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitalPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print('-' * 20)
    print('Site: {0}  Login: {1}  Paytype: {2}'.format(site, login, paytype))
    try:
        email = misc.getEmail(site)
        GVID = sql.addRandom_GV(site, amount, 'beta')
        sql.add_loyalty_points(email, "20000", 'beta')
        GV_code1 = jibber.getGVcode1(GVID)
        GV_code2 = ''
        if(site=='ES'):
            productPage.getSelectedProductPage(site, '15335', targetContainerID)
        else:
            productPage.getSelectedProductPage(site, '319', targetContainerID)


        productPage.clearCountrySwitchBanner()
        # misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitalPage.proceedToBasket()
        if(site=='ES'):
            basketPage.changeBasketQuantity('2')
        else:
            basketPage.changeBasketQuantity('7')

        basketPage.selectCountry(site)
        basketPage.proceedToOPCGV(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutGvOPC(site, login, loyalty, paytype, GV_code1, GV_code2, '4012000033330026', '150', email)
        orderNo = confirmPage.returnOrderNoOPC(site)
        misc.logOut(wd, site, login)
        failures = jibber.checkPaymentsGv(paytype,orderNo, failures)
    except:
        failures = failures+1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

def gv_opc_checkout_secpay(self, site, login, loyalty, paytype, amount, eyes):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitalPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print('-' * 20)
    print('Site: {0}  Login: {1}  Paytype: {2}'.format(site, login, paytype))
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='SecPay Spend GV {} {}'.format(site, paytype))

        email = misc.getEmail(site)
        GV_ID = sql.addRandom_GV(site, amount, 'beta')
        GV_code1 = jibber.getGVcode1(GV_ID)
        GV_code2 = jibber.getGVcode2(paytype)
        # productPage.getProductPage(site, '319', 'beta')   # Use this if you want to run the test on Beta
        productPage.getProductPageContainer(site, '319', '2314')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitalPage.proceedToBasket()
        basketPage.changeBasketQuantity('10')
        basketPage.selectCountry(site)
        basketPage.proceedToOPCGV(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        # Take a screen shot with the red 'required' fields displayed
        misc.eyes_check_window_fluid(eyes, site, 'Required fields - Login {} Paytype {}'.format(login, paytype))
        opc.runCheckoutGvOPCSecPay(site, login, loyalty, paytype, GV_code1, GV_code2, '4012000033330026', '150', email)
        misc.eyes_check_window_fluid(eyes, site, ' Login {} Paytype {}'.format(login, paytype))

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures+1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

#Purchases GVs from each site logged in and out
def purchase_gv(self, targetContainerID, site, login):
    wd = self.wd
    gvPurchasePage = GiftVoucherPurchasePage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addresspage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    print ('-' * 20)
    print ('GV Test: {0}, {1}'.format(site, login))
    try:
        email = misc.getEmail(site)
        if targetContainerID == 'beta':
            gvPurchasePage.getGVpage(site, targetContainerID)
        else:
            gvPurchasePage.getGVpageContainer(site, targetContainerID)
        gvPurchasePage.accept_gdpr_cookies()
        misc.accept_cookies(wd, site, failures)
        gvPurchasePage.fillGVForm()
        loginPage.loginCheckout(email, 'autotester!!', login)
        payOptsPage.selectPaytype('CC')
        addresspage.gvAddress(site, login)
        braintreePage.braintreeBasic('4012000033330026', '150', site)
        gv_code = confirmPage.returnGVcode(site)
        misc.logOut(wd, site, login)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        failures = jibber.checkGvPurchase(site, gv_code, failures)
    except:
        failures = failures+1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

#Purchases GVs from each site logged in and out USING SECPAY
def purchase_gv_secpay(self, site, login, eyes):
    wd = self.wd
    gvPurchasePage = GiftVoucherPurchasePage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addresspage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    print ('-' * 20)
    print ('GV Test: {0}, {1}'.format(site, login))
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='SecPay Purchase GV {} {}'.format(site, login))

        email = misc.getEmail(site)
        # gvPurchasePage.getGVpage(site, 'beta')    # Use this if you want to run the test on Beta
        gvPurchasePage.getGVpageContainer(site, '2314')
        gvPurchasePage.accept_gdpr_cookies()
        misc.accept_cookies(wd, site, failures)
        gvPurchasePage.fillGVForm()
        loginPage.loginCheckout(email, 'autotester!!', login)
        payOptsPage.selectPaytype('CC')
        addresspage.gvAddress(site, login)
        braintreePage.braintreeBasicSecPayEmpty()
        misc.eyes_check_window_fluid(eyes, site, 'Required fields - Site {} Login {}'.format(site, login))
        braintreePage.braintreeBasicSecPay('4012000033330026', '150')
        misc.eyes_check_window_fluid(eyes, site, ' Site {} Login {}'.format(site, login))
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1

    except:
        failures = failures+1
        print(traceback.print_exc())
        print ('fell over')
    print ('-' * 20)
    return failures

#Checks that the PCAP address finder is working on each site
def pcap_check(self, targetContainerID, site):
    wd = self.wd
    wd.maximize_window()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        misc.checkAndUpdateStock(site, '38263')
        productPage.getSelectedProductPage(site, '38263', targetContainerID)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        opc.loginBeforeCheckout(email, 'autotester!!', 'LOT', 'CC')
        opc.pcapAddress(site, 'beta')
        pointsUsed = opc.opcLoyalty('LOT', 'NON')
        opc.opcPlaceOrder(site, 'NON', '4012000033330026', '150')
        orderNo = confirm.returnOrderNoOPC(site)
        set_order_status(orderNo,'Suspended')
        failures = jibber.checkPayments(site, 'NA', 'CC', failures, 'no',orderNo)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation on the change name area of the account
def account_change_name_area(self, targetContainerID, site):
    accountPage = AccountPage(self.wd)
    accountNamePage = AccountNamePage(self.wd)
    self.wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        accountPage.getSelectedAccountPage(targetContainerID, site)
        accountPage.accept_gdpr_cookies()
        accountPage.login(email, 'autotester!!')
        accountPage.accept_gdpr_cookies()
        accountPage.openChangeName(site)
        accountNamePage.clearFields()
        accountNamePage.submitForm()
        accountNamePage.submitForm()
        failures = accountNamePage.checkErrorMessages(site, failures)
        accountNamePage.fillFields(email)
        accountNamePage.submitForm()
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation on the address book area of the account
def account_change_address(self, targetContainerID, site):
    self.wd.maximize_window()
    accountPage = AccountPage(self.wd)
    addressBook = AccountAddressBookPage(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        accountPage.getSelectedAccountPage(targetContainerID, site)
        accountPage.login(email, 'autotester!!')
        accountPage.accept_gdpr_cookies()
        accountPage.openAddressBook(site)
        addressBook.addAddress()
        addressBook.expandAddressField(site)
        addressBook.submitAddress()
        addressBook.submitAddress()
        failures = addressBook.checkAddressArea(site, failures)
        addressBook.enterAddressAccount(site)
        addressBook.removeAddress()
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation on the wishlist area of the account and checks that you can add a product to a wishlist
def account_change_wishlist(self, targetContainerID, site):
    accountPage = AccountPage(self.wd)
    wishlistPage = AccountWishlistPage(self.wd)
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        accountPage.getSelectedAccountPage(targetContainerID, site)
        accountPage.accept_gdpr_cookies()
        accountPage.login(email, 'autotester!!')
        misc.accept_cookies(wd, site, failures)
        accountPage.openWishlists(site)
        wishlistPage.checkWishlistRemoved()
        failures = wishlistPage.addWishlist(site, failures)
        failures = wishlistPage.editWishlist(site, failures)
        failures = wishlistPage.addProductToWishlist(site, failures)
        wishlistPage.deleteWishlist(site)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks the status of an order and switch between 'Approved' and 'Suspended'
def switch_order_status(orderNo):
    orderStatus = sql.sql_get_order_status(orderNo)
    if orderStatus == "Approved":
        #Update the order to be Suspended - Blacklist address
        sql.sql_update_order_status(orderNo,1,40)
        print("New order status: Suspended")
    elif orderStatus == "Suspended":
        #Update the order to be Approved
        sql.sql_update_order_status(orderNo, 2, 0)
        print("Order status: Approved")
    else:
        print("An error occur when switching order status")

#Set the status of an order - 'Approved' or 'Suspended'
def set_order_status(orderNo,status):
    if status == "Approved":
        #Update the order to be Approved
        sql.sql_update_order_status(orderNo, 2, 0)
        print("New order status (SQL): Approved")
    elif status == "Suspended":
        #Update the order to be Suspended - Blacklist address
        sql.sql_update_order_status(orderNo, 1, 40)
        print("Order status (SQL): Suspended")
    else:
        print("Error - Status was not set via SQL!")

#Checks if an order with 'Suspended' or 'Approved' status from account can be canceled
def account_cancel_order(self, targetContainerID, site, prodId, login, paytype, card,status):
    accountPage = AccountPage(self.wd)
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    opc = OnePageCheckout(self.wd)

    wd = self.wd
    wd.maximize_window()
    failures = 0
    print('-' * 20)
    print('Site: {0} Order Status: {1}'.format(site, status))
    try:
        if site == 'AU':
            email = 'auto.testercomau@lovehoney.co.uk'
        else:
            email = misc.getEmailCheckout(site)
        productPage.getSelectedProductPage(site, prodId, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        opc.runCheckoutOPC(site, login, 'NON', paytype, card, '150', email, 'na')
        orderNo = confirmPage.returnOrderNoOPC(site)
        print(orderNo)
        sql.sql_get_order_status(orderNo)
        set_order_status(orderNo,status)
        sql.sql_get_order_status(orderNo)
        accountPage.getAccountPage(site,'beta')
        accountPage.cancelOrderFromAccount(orderNo,failures)
        accountPage.logOut(site,'LIN')
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Same as account_change_name_area() but for mobile
def mobile_account_change_name_area(self,  targetContainerID, site):
    accountPage = AccountPage(self.wd)
    accountNamePage = AccountNamePage(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        accountPage.accept_gdpr_cookies()
        accountPage.login(email, 'autotester!!')
        accountPage.openChangeNameMobile(site)
        accountNamePage.clearFields()
        accountNamePage.submitForm()
        accountNamePage.submitForm()
        failures = accountNamePage.checkErrorMessages(site, failures)
        accountNamePage.fillFields(email)
        accountNamePage.submitForm()
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Same as account_change_address() but for mobile
def mobile_account_change_address(self, targetContainerID, site):
    accountPage = AccountPage(self.wd)
    addressBook = AccountAddressBookPage(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)

        accountPage.accept_gdpr_cookies()
        accountPage.accept_gdpr_cookies()
        accountPage.login(email, 'autotester!!')
        accountPage.openAddressBookMobile(site)
        addressBook.addAddressMobile()
        addressBook.expandAddressFieldMobile(site)
        addressBook.submitAddressMobile()
        addressBook.submitAddressMobile()   # Need to submit twice to get all error messages to show
        failures = addressBook.checkAddressArea(site, failures)
        addressBook.enterAddressAccountMobile(site)
        addressBook.removeAddressMobile()
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Same as account_change_wishlist() but for mobile
def mobile_account_change_wishlist(self,  targetContainerID, site):
    accountPage = AccountPage(self.wd)
    wishlistPage = AccountWishlistPage(self.wd)
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        email = misc.getEmail(site)
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        accountPage.accept_gdpr_cookies()
        accountPage.login(email, 'autotester!!')
        accountPage.openWishlistsMobile(site)
        wishlistPage.checkWishlistRemoved()
        failures = wishlistPage.addWishlistMobile(site, failures)
        failures = wishlistPage.editWishlistMobile(site, failures)
        failures = wishlistPage.addProductToWishlistMobile(site, failures)
        wishlistPage.deleteWishlistMobile(site)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation on the returns page
def returns_page_check(self, site):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        accountslib.getReturnsPage(wd, site)
        failures = val.returns_page_check(wd, site, failures)
        failures = accountslib.returns_page_fill(wd, site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation at the address page of the checkout process
def checkout_basic_validation(self, site):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.getEmail(site)
        productPage.getProductPage(site, '319', 'beta')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', 'LOT')
        payOptsPage.selectPaytype('CC')
        failures = addressPage.checkAddressForm(site, failures)
        failures = braintreePage.check_braintree_validation(site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks form validation for the GV checkout process
def checkout_gv_validation(self, targetContainerID, site):
    wd = self.wd
    gvPage = GiftVoucherPurchasePage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    cardAddressPage = CardholderAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.getEmail(site)
        if targetContainerID == 'beta':
            gvPage.getGVpage(site, targetContainerID)
        else:
            gvPage.getGVpageContainer(site, targetContainerID)
        gvPage.accept_gdpr_cookies()
        misc.accept_cookies(wd, site, failures)
        failures = gvPage.checkGvFormValidation(site, failures)
        gvPage.fillGVForm()
        loginPage.loginCheckout(email, 'autotester!!', 'LOT')
        payOptsPage.selectPaytype('CC')
        failures = cardAddressPage.checkCardholderAddressValidation(site, failures)
        cardAddressPage.enterCardholderAddress(site)
        braintreePage.braintreeBasic('4012000033330026', '150', site)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that you can create an account and checks the form validation whilst doing so
def create_account(self, targetContainerID, site):
    wd = self.wd
    wd.maximize_window()
    accountPage = AccountPage(self.wd)
    jibber = Jibber(self.wd)
    createAccountPage = CreateAccountPage(self.wd)
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.random_email()
        accountPage.getSelectedAccountPage(targetContainerID, site)
        misc.accept_cookies(wd, site, failures)
        accountPage.enterEmailNewAccount(email)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.createAccount(site, email)
        failures = createAccountPage.checkCreateAccountForm(site, failures)
        createAccountPage.createAccountFormFill()
        createAccountPage.setScreenName(email)
        misc.logOut(wd, site, 'LIN')
        accountPage.login(email, 'autotester!!')
        failures = createAccountPage.createAccountCompleteCheck(site, failures)


    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def gdpr_opt_in_checks(self, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    accountPage = AccountPage(self.wd)
    createAccountPage = CreateAccountPage(self.wd)
    jibber = Jibber(self.wd)
    try:
        email = accountPage.randomEmail()
        accountPage.getAccountPage(site, 'beta')
        accountPage.accept_gdpr_cookies()
        accountPage.enterEmailNewAccount(email)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.createAccount(site, email)
        createAccountPage.createAccountFormFill()
        jibber.getAccountFinder('beta')
        jibber.findAccount(email)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
        shotname = misc.nameScreenshotTest(site, 'gpdr_opt_in_check')
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    return failures

#Checks you can use the special order functionality on jibber
#method parameter: mir - mirror website path, man - manual order path
def special_order(self, site, method):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ("Site: {0}, Method: {1}".format(site, method))
    try:
        failures = jibber.special_order(wd, site, method, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks you can save a card from a payment and then use it on another payment
def saved_card_checkout(self, site, card, cvv):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    accountPage = AccountPage(self.wd)
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.getEmail(site)
        productPage.getProductPage(site, '319', 'beta')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', 'LIN')
        payOptsPage.setLoyaltyNon()
        payOptsPage.selectPaytype('CC')
        addressPage.checkoutAddress(site, 'LIN', 'CC')
        braintreePage.braintreeSaveCard(card, cvv, site)
        print('---Finished saving card from payment---')
        orderNo = confirmPage.returnOrderNo(site)
        productPage.getProductPage(site, '319', 'beta')
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        payOptsPage.setLoyaltyNon()
        payOptsPage.selectSaveCard()
        addressPage.checkoutAddress(site, 'LIN', 'CC')
        braintreePage.useSaveCard(card, site)
        orderNo = confirmPage.returnOrderNo(site)
        set_order_status(orderNo,'Suspended')
        failures = jibber.checkPayments(site, 'NON', 'CC', failures, 'no',orderNo)
        accountPage.getAccountPage(site, 'beta')
        failures = accountPage.removeCard(site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures


#Checks you can save a card from a payment and then use it on another payment
def opc_saved_card_checkout(self, targetContainerID, site, card, cvv):
    wd = self.wd
    wd.maximize_window()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    accountPage = AccountPage(self.wd)
    jibber = Jibber(self.wd)
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.getEmailCard(site)
        if(site=='ES'):
            productPage.getSelectedProductPage(site, '15335', targetContainerID)
        else:
            productPage.getSelectedProductPage(site, '319', targetContainerID)

        productPage.accept_gdpr_cookies()
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', 'CC')
        opc.save_card_opc(card, cvv, site)
        print('---Finished saving card from payment---')
        if targetContainerID == 'beta':
            if(site=='ES'):
                productPage.getProductPage(site, '15335', targetContainerID)
            else:
                productPage.getProductPage(site, '319', targetContainerID)
        else:
            if targetContainerID == 'beta':
                if (site == 'ES'):
                    productPage.getProductPage(site, '15335', targetContainerID)
                else:
                    productPage.getProductPage(site, '319', targetContainerID)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        opc.use_save_card_opc(site)
        orderNo = confirm.returnOrderNoOPC(site)
        set_order_status(orderNo, 'Suspended')
        failures = jibber.checkPayments(site, 'NON', 'CC', failures, 'no', orderNo)
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        failures = accountPage.removeCard(site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def cleanupSavedCards(self, targetContainerID, failedContryList, failures):
    accountPage = AccountPage(self.wd)

    print('Removing saved cards after test fail...')
    for site in failedContryList:
        print(site)

        email = misc.getEmail(site)

        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)

        accountPage.login(email, 'autotester!!')

        failures = accountPage.removeCard(site, failures)

    return failures

#Checks that the cancel/void whole order functionality works correctly in jibber
def void_stock_check(self,  targetContainerID, site):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ("Site: {0}".format(site))
    try:
        email = misc.getEmail(site)
        print ('Before:')
        stockBefore = sql.returnStockAll()
        sql.printStockNos(stockBefore)
        productPage.getSelectedProductPage(site, '34002', targetContainerID)
        misc.check_country_screen(wd)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', 'CC')
        pointsUsed = opc.runCheckoutOPC(site, 'LIN', 'NON', 'CC', '4012000033330026', '150', email, 'na')
        orderNo = confirm.returnOrderNoOPC(site)
        print ('-' * 10)
        print ('After:')
        stockAfter = sql.returnStockAll()
        sql.printStockNos(stockAfter)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.enterOrderNo(orderNo)
        jibber.refundOrder()
        print ('-' * 10)
        print ('After Refund:')
        stockAfterRefund = sql.returnStockAll()
        sql.printStockNos(stockAfterRefund)
        print ('-' * 10)
        failures = val.check_stock_all(stockBefore, stockAfter, stockAfterRefund, site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures


#Same as returns_page_check() but for mobile
def mobile_returns_page_check(self, site):
    wd = self.wd
    #wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        accountslib.getReturnsPage(wd, site)
        failures = val.returns_page_check_mobile(wd, site, failures)
        failures = accountslib.returns_page_fill(wd, site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that all of the help pages give a 200 response when requested
def help_page_check(self, targetContainerID, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        if targetContainerID == 'beta':
            misc.getHelpPage(wd, site)
        else:
            misc.getHelpPageContainer(wd, site, targetContainerID)

        failures = misc.checkPageLinks(wd, '//*[@id="main"]', 'a', failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that all of the forum pages give a 200 response when requested
def forum_page_check(self, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        misc.getForumHome(wd)
        failures = misc.checkPageLinks(wd, '//*[@id="content"]', 'a', failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that buy the set products on the front end correctly link to the right products
def buy_the_set_front_end(self):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    try:
        failures = product.buy_the_set_front_end_check(wd, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that buy the set can be set up on jibber
def buy_the_set_back_end(self):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    try:
        failures = jibber.buy_the_set(wd, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that you can purchase products from someones wishlist without revealing their address
def wishlist_purchase(self):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    try:
        failures = checkoutlib.wishlist_purchase(wd, failures)
        orderNo = checkoutlib.returnOrderNo(wd, 'UK')
        failures = jibber.check_wishlist_purchase(wd, orderNo, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Chceks that the pages on brochureware sites provide a 200 response when requested
def brochureware_page_check(self, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Site: {0}'.format(site))
    try:
        misc.getBrochurewareHomePage(wd, site)
        failures = misc.checkPageLinks(wd, '//*[@id="header"]', 'a', failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def jibber_checkOrderStatus(self, status):
    wd = self.wd
    jibber = Jibber(self.wd)
    wd.maximize_window()
    failures = 0
    suspendedReason = ''
    suspendedReasonIds = [39, 40, 31, 3, 1, 31, 2, 16, 13, 5, 19, 9, 7, 8, 35, 34, 36, 37, 38, 41, 42, 30]
    print('-' * 20)
    try:
        jibber.getJibber('beta')
        jibber.login("qa.autotest", "trying2FIX!")

        if status == 'Approved':
            # Cast generator to list
            order = list(sql.sql_get_approved_orderId())
            orderNo = order.pop()
            print('Order No: ',orderNo)
            print('SQL status: Approved')
        elif status == 'Suspended':
            # Select an order from DB based on a random reason
            id = random.choice(suspendedReasonIds)
            # Cast generator to list
            order = list(sql.sql_get_suspended_orderId(id))
            while not order:
                print('No order with suspended reason: ', sql.get_suspended_reason(id))
                print('Retry with another suspended reason...')
                id = random.choice(suspendedReasonIds)
                order = list(sql.sql_get_suspended_orderId(id))
            orderNo = order.pop()
            suspendedReason = sql.get_suspended_reason(id)
            print('Order No: ', orderNo)
            print('SQL status: Suspended | Suspend Reason : {}'.format(suspendedReason))
        elif status == 'Cancelled':
            # Cast generator to list
            order = list(sql.sql_get_canceled_orderId())
            orderNo = order.pop()
            print('Order No: ', orderNo)
            print('SQL status: Cancelled')
        else:
            failures += 1

        jibber.enterOrderNo(orderNo)
        failures = jibber.checkOrderStatusUI(status, suspendedReason, failures)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print('-' * 20)
    return failures

def jibber_suspend_order(self, status):
    wd = self.wd
    jibber = Jibber(self.wd)
    wd.maximize_window()
    failures = 0
    suspendedReason = ''
    suspendedReasonIds = [39, 40, 31, 3, 1, 31, 2, 16, 13, 5, 19, 9, 7, 8, 35, 34, 36, 37, 38, 41, 42, 30]
    print('-' * 20)
    try:
        jibber.getJibber('beta')
        jibber.login("qa.autotest", "trying2FIX!")

        if status == 'Approved':
            # Cast generator to list
            order = list(sql.sql_get_approved_orderId())
            orderNo = order.pop()
            print('Order No: ', orderNo)
            print('SQL status: Approved')
        elif status == 'Suspended':
            # Select an order from DB based on a random reason
            id = random.choice(suspendedReasonIds)
            # Cast generator to list
            order = list(sql.sql_get_suspended_orderId(id))
            while not order:
                print('No order with suspended reason: ', sql.get_suspended_reason(id))
                print('Retry with another suspended reason...')
                id = random.choice(suspendedReasonIds)
                order = list(sql.sql_get_suspended_orderId(id))
            orderNo = order.pop()
            suspendedReason = sql.get_suspended_reason(id)
            print('Order No: ', orderNo)
            print('SQL status: Suspended | Suspend Reason : {}'.format(suspendedReason))
        elif status == 'Cancelled':
            # Cast generator to list
            order = list(sql.sql_get_canceled_orderId())
            orderNo = order.pop()
            print('Order No: ', orderNo)
            print('SQL status: Cancelled')
        else:
            failures += 1
        # Select a random reason to suspend an order in Jibber
        suspendedReasonId = random.choice(suspendedReasonIds)
        jibber.enterOrderNo(orderNo)
        failures = jibber.suspendOrderUI(status, suspendedReasonId, failures)
        # Check if the order status was updated properly
        if status == 'Approved':
            suspendedReason = sql.get_suspended_reason(suspendedReasonId)
            print('SQL status: Suspended | Suspend Reason : {}'.format(suspendedReason))
            failures = jibber.checkOrderStatusUI('Suspended', suspendedReason, failures)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print('-' * 20)
    return failures

#Checks that you can change the country of an address on jibber to all provided countries
def jibber_check_address(self):
    wd = self.wd
    jibber = Jibber(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    try:
        failures = jibber.jibberCheckAddress(failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

#Checks that the mobile hamburger behaves correctly
def mobile_hamburger(self, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    try:
        failures = product.mobile_hamburger_check(wd, site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def header_tests(self, site, expectedlinks, cs_expected_link, live_chat_link_text, live_chat_pop_message_open, live_chat_pop_message_closed, accountlinkurl, search_list,
                 firstexpectURL, secondexpectURL, thirdexpectURL, basketURL, logoURL):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Doing the header tests')
    try:
        misc.getHome(wd, site)
        print ("Checking page links:")
        failures = val.all_page_links(wd, expectedlinks, failures)
        print ("Checking searchbox:")
        failures = val.searchbox(wd, search_list, firstexpectURL, secondexpectURL, thirdexpectURL, "NO", failures)
        print ("Checking customer care link:")
        failures = val.customer_service_link(wd, cs_expected_link, failures)
        print ("Checking live chat link:")
        failures = val.live_chat_link(wd, live_chat_link_text, live_chat_pop_message_open, live_chat_pop_message_closed, failures)
        print ("Checking account link:")
        failures = val.youraccount(wd, accountlinkurl, failures)
        print ("Checking wishlist link:")
        failures = val.wishlist(wd, accountlinkurl, failures)
        print ("Checking basket link:")
        failures = val.basket(wd, basketURL, failures)
        print ("Checking LH logo:")
        failures = val.logo(wd, logoURL, failures)
        print ("Checking the site switcher:")
        failures = val.siteswitcher(wd, site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def user_login_time(self, targetContainerID, site):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    print ('-' * 20)
    print ('Doing the user login time test')
    try:
        if targetContainerID == 'beta':
            accountslib.getAccountPage(wd, site)
        else:
            accountslib.getAccontPageContainer(wd, site, targetContainerID)

        accountslib.accountLogin(wd, "auto.testercouk@lovehoney.co.uk", "autotester!!")
        misc.check_country_screen(wd)
        misc.logOut(wd, site, "LIN")
        date_time = misc.getTime()
        time.sleep(2)
        login_time = sql.getUserLoginTime(1230695)
        failures = accountslib.checking_user_account_login_time(date_time, login_time, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print ('-' * 20)
    return failures

def paginationNonUK(self, page):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        misc.getPage(wd, page)
        all_links = misc.getPageElementsHttpLinks(wd, "//*[@id='products']/div[3]/div", 'a')
        failures = misc.checkAllGivenHrefs(wd, all_links, 'href', failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def paginationUK(self, page):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        misc.getPage(wd, page)
        all_links = misc.getPageElementsHttpLinks(wd, "//div[@class='product-list-pagination__page-numbers']", 'a')
        failures = misc.checkAllGivenHrefs(wd, all_links, 'href', failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def expected404(self, page):
    wd = self.wd
    #wd.maximize_window()
    failures = 0
    try:
        failures = misc.checkForExpected404(wd, page, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def braintree_auth_logged_in_diff_card_address(self, targetContainerID, site, loyalty, paytype, card_number, cvv):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    opc = OnePageCheckout(self.wd)
    confirm = ConfirmationPage(self.wd)
    jibber = Jibber(self.wd)
    wd.maximize_window()
    failures = 0
    product_name = 'BASICS Mini Vibrator Silver 5 Inch Silver'
    try:
        print (20 * "-")
        print ("Card number = {0} Site = {1} Loyalty Points = {2}".format(card_number, site, loyalty))
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        misc.getHome(wd, site)
        productPage.getSelectedProductPage(site, '319', targetContainerID)
        productPage.clearCountrySwitchBanner()
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', paytype)
        pointsUsed = opc.runCheckoutOPC(site, 'LIN', loyalty, paytype, card_number, cvv, email, 'na')
        orderNo = confirm.returnOrderNoOPC(site)
        braintreePage.logOut(site, "LIN")
        failures = jibber.jibber_check_product(product_name,orderNo, failures)
        failures = jibber.checkPayments(site, loyalty, paytype, failures, 'no', orderNo)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def braintree_declined_logged_in_diff_card_address(self, targetContainerID, site, loyalty, paytype, card_number, cvv, error_text1, error_text2):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    opc = OnePageCheckout(self.wd)
    wd.maximize_window()
    failures = 0
    try:
        print (20 * "-")
        print ("Card number = {0} Site = {1} Loyalty Points = {2}".format(card_number, site, loyalty))
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, "20000", 'beta')
        misc.getHome(wd, site)

        '''
        Using a different product for the US and ES as 22112 isn't available in these countries
        Both products need their prices setting to 2001.00 after a rebase so that the Braintree declined
        action occurs.  Set the price in: https://productcommissioning.lhapps.beta
        '''

        if(site == 'CM' or site == 'ES'):
            misc.checkAndUpdateStock(site, '38736')
        else:
            misc.checkAndUpdateStock(site, '22112')

        if(site == 'CM' or site == 'ES'):
            productPage.getSelectedProductPage(site, '38736', targetContainerID)
        else:
            productPage.getSelectedProductPage(site, '22112', targetContainerID)

        productPage.clearCountrySwitchBanner()
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', paytype)
        pointsUsed = opc.runCheckoutOPC(site, 'LIN', loyalty, paytype, card_number, cvv, email, 'na')
        failures = opc.checkFailedPayment(site, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def braintree_auth_logged_out_dif_card_address(self, targetContainerID, site, paytype, card_number, cvv):
   wd = self.wd
   productPage = ProductPage(self.wd)
   interstitialPage = InterstitialPage(self.wd)
   basketPage = BasketPage(self.wd)
   loginPage = LoginPage(self.wd)
   payOptsPage = PaymentOptionsPage(self.wd)
   addressPage = DeliveryAddressPage(self.wd)
   braintreePage = BraintreeCardPage(self.wd)
   opc = OnePageCheckout(self.wd)
   confirm = ConfirmationPage(self.wd)
   jibber = Jibber(self.wd)
   wd.maximize_window()
   failures = 0
   product_name = 'BASICS Mini Vibrator Silver 5 Inch Silver'
   try:
       print (20 * "-")
       print ("Card number = {0} Site = {1}".format(card_number, site))
       email = misc.getEmail(site)
       productPage.getSelectedProductPage(site, '319', targetContainerID)
       productPage.clearCountrySwitchBanner()
       productPage.accept_gdpr_cookies()
       productPage.addProductToBasket()
       interstitialPage.proceedToBasket()
       basketPage.selectCountry(site)
       basketPage.proceedToOPC(paytype)
       opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', paytype)
       pointsUsed = opc.runCheckoutOPC(site, 'LIN', 'NON', paytype, card_number, cvv, email, 'na')
       orderNo = confirm.returnOrderNoOPC(site)
       misc.logOut(wd, 'UK', 'LIN')
       failures = jibber.jibber_check_product(product_name,orderNo, failures)
       failures = jibber.checkPayments(site, 'NA', paytype, failures, 'no',orderNo)
   except:
       print ('fell over')
       print(traceback.print_exc())
       failures = failures + 1
   return failures

def braintree_declined_logged_out(self, targetContainerID, site, paytype, card_number, cvv, login, loyalty):
   wd = self.wd
   wd.maximize_window()
   wd.delete_all_cookies()
   productPage = ProductPage(self.wd)
   interstitialPage = InterstitialPage(self.wd)
   basketPage = BasketPage(self.wd)
   loginPage = LoginPage(self.wd)
   paymentOptsPage = PaymentOptionsPage(self.wd)
   addressPage = DeliveryAddressPage(self.wd)
   braintreePage = BraintreeCardPage(self.wd)
   opc = OnePageCheckout(self.wd)
   failures = 0

   try:
       print (20 * "-")
       print ("Card number = {0} Site = {1}".format(card_number, site))
       email = misc.getEmail(site)
       #sql.add_loyalty_points(email, "20000", 'beta')
       #misc.getHome(wd, site)

       if (site == 'CM' or site == 'ES'):
           misc.checkAndUpdateStock(site, '38736')
       else:
           misc.checkAndUpdateStock(site, '22112')

       if (site == 'CM' or site == 'ES'):
           productPage.getSelectedProductPage(site, '38736', targetContainerID)
       else:
           productPage.getSelectedProductPage(site, '22112', targetContainerID)

       productPage.clearCountrySwitchBanner()
       productPage.accept_gdpr_cookies()
       misc.check_country_screen(wd)
       misc.accept_cookies(wd, site, failures)
       productPage.addProductToBasket()
       interstitialPage.proceedToBasket()
       basketPage.selectCountry(site)
       basketPage.proceedToOPC(paytype)
       opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
       pointsUsed = opc.runCheckoutOPC(site, login, loyalty, paytype, card_number, cvv, email, 'na')
       failures = opc.checkFailedPayment(site, failures)
   except:
       print ('fell over')
       print(traceback.print_exc())
       failures = failures + 1
   return failures

def Braintree_Suspend_reason_tests(self, site, cardnumber, CV2, productnumber, fraud, suspendlineone, suspendpostcode, overide_sus_3ds, addressline1, fr_suspend_rule):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    payOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirm = ConfirmationPage(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-'*20)
    try:
        print ("Card number = {0} Site = {1} CVV = {2}".format(cardnumber, site, CV2))
        productPage.getProductPage(site, productnumber, 'beta')
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        failures = loginPage.useFraudMail(fraud, failures)
        payOptsPage.selectPaytype('CC')
        addressPage.enterSuspendedAddress(site, suspendlineone, suspendpostcode, addressline1)
        addressPage.sameAddress('YES')
        braintreePage.braintreeBasic(cardnumber, CV2, site)
        ordernumber = confirm.returnOrderNo(site)
        order_status = sql.getOrderStatus(ordernumber)
        suspend_reason = sql.getSuspendreason(ordernumber)
        cv2_message = sql.getPaymenttext(ordernumber)
        failures = misc.check_if_order_suspended(site, order_status, overide_sus_3ds, failures)
        failures = misc.check_order_suspend_reason(site, fraud, suspend_reason, overide_sus_3ds, fr_suspend_rule, failures)
        failures = misc.check_order_payment_text(CV2, cv2_message, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def path_disclosure_in_feeds(self, link):
    wd = self.wd
    #wd.maximize_window()
    failures = 0
    try:
        print (20 * "-")
        print("Testing this link: {0}".format(link))
        failures = misc.checkForExpected404(wd, link, failures)
    except:
        print ('fell over)')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def Post_checkout_bribe_test(self, site, product_id, ccnumber, addressline1):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    wd.maximize_window()
    failures = 0
    try:
        print ("The web site this test is running on is %s" % site)
        random_email = misc.random_email()
        product.getProductPage(wd, site, product_id)
        misc.accept_cookies(wd, site, failures)
        product.addProductToBasket(wd, site)
        checkoutlib.loginCheckout(wd, random_email, "nope", "LOT")
        checkoutlib.selectPaytype(wd, "CC")
        checkoutlib.suspend_reasons_address_add(wd, site, "no", "no", addressline1)
        checkoutlib.braintree_checkout_basic(wd, ccnumber, "150", site)
        failures = misc.which_post_checkout_button(wd, site, failures)
        failures = misc.check_for_railo_or_404(wd, failures)
        failures = misc.find_string_on_page(wd, random_email, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_images(self, page):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        wd.get(page)
        wd.refresh()
        all_links = misc.getPageElementsHttpLinks(wd, "no", "img")
        failures = misc.checkAllGivenHrefs(wd, all_links, 'src', failures)
        return  failures
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
        shotname = misc.nameScreenshotTest('UK', 'checking_page_images')
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    return failures

def forum_page_search(self, targetContainerID):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        if targetContainerID == 'beta':
            misc.get_community_page(wd)

            # Parameters = site lang to search,  URL that should be returned for the cock ring search, URL that should be returned for the bum search,
            # URL that should be returned for another search this is not used for eng lang sites hence set to nope
            failures = val.searchbox(wd, 'eng', 'https://lovehoneycouk.beta/community/forums/search/?q=cock+ring',
                                     'https://lovehoneycouk.beta/community/forums/search/?q=bum', 'nope', 'YES',
                                     failures)
        else:
            misc.get_community_pageContainer(wd, targetContainerID)
            failures = val.searchbox(wd, 'eng', 'https://lovehoneycouk-{}.container.ci/community/forums/search/?q=cock+ring'.format(targetContainerID),
                                     'https://lovehoneycouk-{}.container.ci/community/forums/search/?q=bum'.format(targetContainerID), 'nope', 'YES',
                                     failures)


    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def forum_add_post_thread(self, targetContainerID):
    wd = self.wd
    wd.maximize_window()
    forumPage = ForumPage(self.wd)
    failures = 0
    try:
        forumPage.account_login(targetContainerID, 'auto.testercouk@lovehoney.co.uk', 'autotester!!')
        time.sleep(5)
        forumPage.create_new_thread(targetContainerID, "I am going to add a post to this thread", "First post content")
        failures = forumPage.the_rest(failures)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def loyalty_section_check(self, site, points, email, pw, login, products):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentpage = PaymentOptionsPage(self.wd)

    failures = 0
    try:
        wd = self.wd
        failures = 0
        print('-' * 20)
        # SCRIPT IN CURRENT FORM REQUIRES 10000 POINTS TO BE USED
        sql.add_loyalty_points(email, points, 'beta')
        productPage.getProductPage(site, products, 'beta')
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToCheckout()
        loginPage.loginCheckout(email, 'autotester!!', login)
        failures = failures + paymentpage.test_loyalty_title_in_checkout()
        failures = failures + paymentpage.test_loyalty_spend_all_box()
        print("Running tests for loyalty step down function")
        failures = failures + paymentpage.test_loyalty_step_down()
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def loyalty_section_check_live(self, site, email, pw, products):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    productPage = ProductPage(self.wd)
    opcPage = OnePageCheckout(self.wd)
    basketPage = BasketPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    failures = 0
    try:
        wd = self.wd
        failures = 0
        print('-' * 20)
        # SCRIPT IN CURRENT FORM REQUIRES 10000 POINTS TO BE USED
        productPage.getProductPageLive(site, products)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        #Increasingly BS
        misc.increasingly_on_add_to_basket(wd)
        try:
            interstitialPage.proceedToBasket()
        except:
            print("Increasinly is on so no proceed to basket")
        basketPage.selectCountry(wd)
        basketPage.proceedToOPC("CC")
        checkoutlib.loginOPC(wd, email, pw, 'LIN', 'CC')
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_title_in_checkout(failures)
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_step_up(2, failures)
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_spend_all_box(failures)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def winnow_tests(self, url):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        misc.getPage(wd, url)
        failures = product.testing_winnowing(wd, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def winnow_tests_react(self, url):
    wd = self.wd
    win = Winnow(self.wd)
    wd.maximize_window()
    failures = 0
    try:
        #Wont need this get below once the new winnow is live
        # win.flickWinnowSwitch(url)
        # time.sleep(3)
        makeURL = ("https://lovehoneycouk{0}/sexy-lingerie/sexy-clothing".format(url))
        print(makeURL)
        misc.getPage(wd, makeURL)
        failures = product.testing_winnowing_react(wd, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def applitools_opc_container(self, site, login, paytype, eyes, container):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=site + login + ' opc_checkout_container')
        email = misc.getEmail(site)
        product.getProductPageContainer(wd, site, '319', container)
        misc.check_country_screen(wd)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        #Checking the basket page
        basketPage.selectCountry(site)
        misc.eyes_check_window_fluid(eyes, site, 'OPCBasketPage')
        basketPage.proceedToOPC(paytype)
        #Checking the OPC login screen
        misc.eyes_check_window_fluid(eyes, site, 'OPC_login_screen')
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        #Checking the main OPC screen
        misc.eyes_check_window_fluid(eyes, site, 'OPC_main_screen')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

def applitools_opc(self, site, login, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=site + login + ' opc_checkout')
        email = misc.getEmail(site)
        product.getProductPage(wd, site, '319')
        misc.check_country_screen(wd)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        #Checking the basket page
        basketPage.selectCountry(site)
        misc.eyes_check_window_fluid(eyes, site, 'OPCBasketPage')
        basketPage.proceedToOPC(paytype)
        #Checking the OPC login screen
        misc.eyes_check_window_fluid(eyes, site, 'OPC_login_screen')
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        #Checking the main OPC screen
        time.sleep(5)
        misc.eyes_check_window_fluid(eyes, site, 'OPC_main_screen')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def applitools_confirmation_page_opc(self, targetContainerID, site, login, paytype, eyes, path, container, width, height):
    wd = self.wd
    #wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=path)
        email = misc.getEmail(site)
        productPage.getSelectedProductPage(site, '319', targetContainerID)
        URL = wd.current_url
        print(URL)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, "UK", failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        time.sleep(5)
        opc.runCheckoutOPC(site, login, 'NON', paytype, '4012000033330026', '150', email, 'na')
        # Checking the confirmation page
        time.sleep(15)
        misc.eyes_check_window_fluid(eyes, site, 'The_OPC_confirmation_page_{0}'.format(path))
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures

def applitools_all_opc_pages(self, targetContainerID, site, login, paytype, eyes, path, container, width, height):
    wd = self.wd
    #wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=path, viewport_size={'width': width, 'height': height})
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, '20000', 'beta')
        productPage.getSelectedProductPage(site, '20464', targetContainerID)
        URL = wd.current_url
        print(URL)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, "UK", failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        time.sleep(5)
        misc.eyes_check_window_fluid(eyes, site, 'The_OPC_basket_page_{0}'.format(path))

        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        # basketPage.waitForIdClickable("shippingOption2")    # Waiting for shipping fields to load before taking a screen shot
        time.sleep(5)
        misc.eyes_check_window_fluid(eyes, site, 'No_order_details_{0}'.format(path))

        checkoutlib.expand_opc_details_section(wd)
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'With_order_details_{0}'.format(path))

        time.sleep(10)
        checkoutlib.select_one_working_day_opc_UK(wd)
        opc.submit_order_OPC()
        misc.eyes_check_window_fluid(eyes, site, 'To_show_errors_{0}'.format(path))

        print('Selecting cheapest shipping option...')
        # checkoutlib.select_cheapest_delivery_opc_UK(wd)
        print('Cheapeast shipping option selected')
        opc.runCheckoutOPC_applitools(site, login, 'NON', paytype, '4012000033330026', '150', email)
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Filled_out_with_address_and_card_{0}'.format(path))

        #If its a guest order will need this
        try:
            opc.confirm_address_details()
            time.sleep(7)
            misc.eyes_check_window_fluid(eyes, site, 'Filled_out_with_confirmed_address_and_card_{0}'.format(path))

        except:
            print ('This is a logged in order test so no confirm address button was found')
        opc.submit_order_OPC()
        # Checking the confirmation page
        time.sleep(15)
        # misc.eyes_check_window_fluid(eyes, site, 'The_OPC_confirmation_page_{0}'.format(path))   # Confirmation page checked in opc_check_conf test

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures



def applitools_all_subs_pages(self, targetContainerID, site, login, paytype, eyes, path, width, height):
    wd = self.wd
    wd.delete_all_cookies()
    wd.maximize_window()
    failures = 0
    opc = OnePageCheckout(self.wd)
    subs = Subs_checkout(self.wd)
    accountPage = AccountPage(self.wd)
    basePage = BasePage(self.wd)

    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=path, viewport_size={'width': width, 'height': height})
        print('Open issue - SUBS-445 - Divider appears below content on Subs Checkout page')
        email = misc.getEmail(site)
        #Getting subs landing page
        subs.getSubsCheckout(targetContainerID)
        #Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        #Clicking the subscribe button
        # subs.clickElementByCss(subs.subscribe_button)
        # time.sleep(5)
        subs.waitForXpathClickable("//a[contains(@class, 'subscription-landing__cta-button')]")
        subs.clickElementByXpath("//a[contains(@class, 'subscription-landing__cta-button')]")
        subs.loginBeforeSubsCheckout(email, 'autotester!!', login, paytype)
        # time.sleep(5)
        eyes.check_window('Subs_checkout_{0}'.format(path))
        #Hitting subscribe button to provoke errors
        opc.submit_order_OPC()
        eyes.check_window('Subs_checkout_errors_provoked_{0}'.format(path))
        #Putting in the details for the name section
        randomEmail = subs.addName(login, "Julian", "Bowles")
        opc.runCheckoutOPC_applitoolsSubs(site, login, 'NON', paytype, '4012000033330026', '150', email)
        eyes.check_window('Subs_filled_out__page_{0}'.format(path))
        #Submitting order
        opc.submit_order_OPC()
        # Wait for the order confirmation page to load
        opc.waitForXpathClickable("//a[contains(@class,'message__homepage-link')]")
        eyes.check_window('Subs_confirmation_page_{0}'.format(path))

        # Delete the subscription
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        if(login == 'LOT'):
            accountPage.login(randomEmail, 'lovehoney')
        accountPage.getLastSubscription()
        eyes.check_window('Subscription_details_{0}'.format(path))
        accountPage.clickCancelSubs()
        eyes.check_window('Delete_dialogue_{0}'.format(path))
        accountPage.clickCancelSubsDialog()
        eyes.check_window('Cancel_Configmation_{0}'.format(path))

        # Ok and Logout
        accountPage.clickSubsDeleteConfirmation()
        basePage.logOut('UK', 'LIN')

        time.sleep(5)

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print ('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print ('-' * 20)
    return failures


def applitools_legacy(self, site, login, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    loginPage = LoginPage(self.wd)
    paymentOptsPage = PaymentOptionsPage(self.wd)
    addressPage = DeliveryAddressPage(self.wd)
    braintreePage = BraintreeCardPage(self.wd)
    confirmPage = ConfirmationPage(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=site + login + ' legacy_checkout')
        email = misc.getEmail(site)
        sql.add_loyalty_points(email, '20000', 'beta')
        product.getProductPage(wd, site, '319')
        misc.check_country_screen(wd)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        # Checking the basket page
        basketPage.selectCountry(site)
        misc.eyes_check_window_fluid(eyes, site, 'BasketPageLegacy')
        basketPage.proceedToCheckout()
        #Checking login page
        misc.eyes_check_window_fluid(eyes, site, 'LoginPageLegacy')
        loginPage.loginCheckout(email, 'autotester!!', login)
        pointsUsed = paymentOptsPage.loyaltyInput('NON')
        #Checking payment options page
        misc.eyes_check_window_fluid(eyes, site, 'PaymentOptionsPageLegacy')
        paymentOptsPage.selectPaytype(paytype)
        misc.eyes_check_window_fluid(eyes, site, 'AddressPageLegacy')
        addressPage.checkoutAddress(site, login, paytype)
        misc.eyes_check_window_fluid(eyes, site, 'BraintreeCardPageLegacy')
        braintreePage.useBraintree(site, paytype, 'NON', '4012000033330026', cvv='150')
        misc.eyes_check_window_fluid(eyes, site, 'ConfirmationPageLegacy')
        orderNo = confirmPage.returnOrderNo(site)
        misc.logOut(wd, site, login)
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print('-' * 20)
    return failures

def applitools_returns_page(self, targetContainerID, eyes, site, failures):
    print('Checking: ' + site)
    wd = self.wd
    productPage = ProductPage(self.wd)
    returnsPage = ReturnsPage(self.wd)
    ret = Returns(self.wd)

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name=site)

        print("Checking site = " + site)

        if targetContainerID == 'beta':
            returnsPage.getReturnsPage(site)
        else:
            returnsPage.getReturnsPageContainer(site, targetContainerID)

        productPage.clearCountrySwitchBanner()
        productPage.accept_gdpr_cookies()
        time.sleep(1)
        # misc.eyes_check_window_fluid(eyes, site, 'Empty Returns page')

        ret.clickSendMessage()
        time.sleep(1)
        misc.eyes_check_window_fluid(eyes, site, 'Field error messages')

        ret.fillTextFields()

        misc.select_dropdown_option_css(wd, "select#whyreturn", "Sex Toy Happiness Promise Claim")
        ret.fillReturnText()
        misc.select_dropdown_option_css(wd, "select#whattodo", "Replace the item")
        time.sleep(1)
        misc.eyes_check_window_fluid(eyes, site, 'All fields filled')

        misc.select_dropdown_option_css(wd, "select#whyreturn", "Something is wrong with my order or a product")
        ret.fillReturnText()
        misc.select_dropdown_option_css(wd, "select#whattodo", "Send me the alternative item named below")
        ret.fillAltItem()
        time.sleep(1)
        misc.eyes_check_window_fluid(eyes, site, 'Something is wrong & Send Alternative')

        misc.select_dropdown_option_css(wd, "select#whyreturn", "I changed my mind about my order or a product")
        ret.fillReturnText()
        misc.select_dropdown_option_css(wd, "select#whattodo", "Refund me for the item")
        time.sleep(1)
        misc.eyes_check_window_fluid(eyes, site, 'Changed My Mind & Refund Me')

        ret.clickSendMessage()
        misc.eyes_check_window_fluid(eyes, site, 'Refund instructions')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1

    return failures

def applitools_create_account(self, targetContainerID, eyes, site, failures):
    wd = self.wd
    wd.maximize_window()
    accountPage = AccountPage(self.wd)
    jibber = Jibber(self.wd)
    createAccountPage = CreateAccountPage(self.wd)
    failures = 0
    print('-' * 20)
    print("Site: {0}".format(site))
    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='Create Account Form')

        email = misc.random_email()
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        misc.accept_cookies(wd, site, failures)
        accountPage.enterEmailNewAccount(email)
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.createAccount(site, email)
        misc.eyes_check_window_fluid(eyes, "{} ".format(site), 'Empty Form')
        failures = createAccountPage.checkCreateAccountForm(site, failures)
        misc.eyes_check_window_fluid(eyes, "{} ".format(site), 'Error Fields')
        createAccountPage.applitools_createAccountFormFill(eyes, site)
        misc.eyes_check_window_fluid(eyes, "{} ".format(site), 'Submitted')
        misc.logOut(wd, site, 'LIN')
        accountPage.login(email, 'tester')
        failures = createAccountPage.createAccountCompleteCheck(site, failures)

        # Delete the newly created account
        jibber.getJibber('beta')
        jibber.login('qa.autotest', 'trying2FIX!')
        jibber.deleteAccount('beta', email)

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    print('-' * 20)
    return failures


def applitools_mac_1(self, targetContainerID, site, login, paytype, eyes, path, container, width, height):
    wd = self.wd
    wd.delete_all_cookies()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    mac = MAC(self.wd)
    accountPage = AccountPage(self.wd)
    jibber = Jibber(self.wd)
    try:
        wd = eyes.open(driver=wd, app_name='Applitools', test_name=path)
        email = misc.random_email()
        if targetContainerID == 'beta':
            product.getProductPage(wd, site, '319', targetContainerID)
        else:
            product.getProductPageContainer(wd, site, '319', targetContainerID)
        URL = wd.current_url
        print(URL)
        misc.check_country_screen(wd)
        misc.accept_cookies(wd, site, failures)
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        opc.loginBeforeCheckout(email, 'xx', login, paytype)
        opc.runCheckoutOPC(site, login, 'NON', paytype, '4012000033330026', '150', email, 'na')

        # Checking the MAC page
        misc.eyes_check_window_fluid(eyes, site, 'Non account holder')

        mac.clickCreateAccount()
        misc.eyes_check_window_fluid(eyes, site, 'Click without password entered')

        mac.enterPassword("autotester!!")
        misc.eyes_check_window_fluid(eyes, site, 'Password set')

        mac.clickCreateAccount()

        mac.clickMacRadio("relationship-other")
        mac.fillOtherText("relationship", "Hermit")
        mac.clickMacRadio("gender-other")
        mac.fillOtherText("gender", "I'm an orange Jelly Baby")
        mac.clickMacRadio("preference-other")
        mac.fillOtherText("preference", "I like bouncing")
        misc.eyes_check_window_fluid(eyes, site, 'Great stuff!! screen')

        mac.clickSaveProfile()
        misc.eyes_check_window_fluid(eyes, site, 'Thanks for signing up! screen')

        # Logging in and checking new account details are correct
        if targetContainerID == 'beta':
            accountPage.getAccountPage(site, 'beta', targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        misc.accept_cookies(wd, site, failures)
        accountPage.login(email, "autotester!!")
        accountPage.openChangeName(site)
        misc.eyes_check_window_fluid(eyes, site, 'Account - Change name page')

        # accountPage.getAboutYourself(site)
        accountPage.getAboutYourselfContainer(site, container) # Swap back to the Beta test when required
        misc.eyes_check_window_fluid(eyes, site, 'Account - Change About Yourself page')

        # Delete the newly created account
        try:
            jibber.getJibber('beta')
            jibber.login('qa.autotest', 'trying2FIX!')
            jibber.deleteAccount('beta', email)
        except:
            print('** Unable to delete account **')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    print('-' * 20)
    return failures

def opc_loyalty_section_check(self, targetContainerID, site, points, email, pw, login, products):
    wd = self.wd
    failures = 0
    opcPage = OnePageCheckout(self.wd)
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)

    print ('-'*20)
    try:
        # SCRIPT IN CURRENT FORM REQUIRES 10000 POINTS TO BE USED AND POSTAGE OPTION TO NOT BE UPDATED
        sql.add_loyalty_points(email, points, 'beta')
        productPage.getSelectedProductPage(site, '319', targetContainerID)
        print("Testing on the {0}".format(site))
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC('CC')
        opc.loginBeforeCheckout(email, pw, login, 'CC')
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_title_in_checkout(failures)
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_step_up(2, failures)
        time.sleep(5)
        failures = opcPage.opc_test_loyalty_spend_all_box(failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def checkpageresponsetime(self, url, how_many_times_to_check_page, threshold):
    wd = self.wd
    basePage = BasePage
    wd.maximize_window()
    failures = 0
    try:
        failures = misc.checkpageloadtime(wd, url, how_many_times_to_check_page, threshold, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
        shotname = misc.nameScreenshotTest('UK', 'page_response_time')
        wd.save_screenshot('test-reports/{0}.png'.format(shotname))
    return failures

def wishlist_purchase_and_sharing_1(self, targetContainerID):
   wd = self.wd
   wd.maximize_window()
   failures = 0
   try:
       #Declare products descriptions that will be used to check correct products are added to wishlist
       if 'beta' in targetContainerID:
            accountslib.getAccountPage(wd, 'UK')
       else:
           accountslib.getAccontPageContainer(wd, 'UK', targetContainerID)
       misc.accept_cookies(wd, 'UK', 0)
       # Login to wishlist test account:
       accountslib.accountLogin(wd, "julian.bowles+totestaccount@lovehoney.co.uk", "lovehoney")
       #Go to wishlist page in test account:
       accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
       #check if any wishlists have been left by a prevoius test run that failed, added below:
       accountslib.delete_all_wishlists(wd, "//div[contains(@class,'wishlist')]/ol/li[1]/ul")
       #Create 5 wishlists for testing(The [0] at the end is to bring the failures back from the array created by the makewishlist function):
       failures = accountslib.makewishlist(wd, 'Monster', failures)[0]
       #Go to wishlist page in test account(The [0] at the end is to bring the failures back from the array created by the makewishlist function):
       accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
       failures = accountslib.makewishlist(wd, 'Redbull', failures)[0]
       #Go to wishlist page in test account(The [0] at the end is to bring the failures back from the array created by the makewishlist function):
       accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
       failures = accountslib.makewishlist(wd, 'Relentless', failures)[0]
       #Go to wishlist page in test account(The [0] at the end is to bring the failures back from the array created by the makewishlist function):
       accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
       failures = accountslib.makewishlist(wd, 'Rockstar', failures)[0]
       #Go to wishlist page in test account:
       accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
       returnArray = accountslib.makewishlist(wd, 'Vee', failures)
       failures = returnArray[0]
       wishlist_id = returnArray[1]
       #Add 3 products to the wishlist
       accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "319", wishlist_id)
       accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "16603", wishlist_id)
       accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "34938", wishlist_id)
       #Check products are on wishlist
       failures = accountslib.check_products_on_wishlist(wd, "BASICS Powerful Mini Vibrator 5 Inch (Silver)", failures)
       failures = accountslib.check_products_on_wishlist(wd, "Lovehoney Silencer Whisper Quiet Classic Vibrator 7 Inch (Pink)", failures)
       failures = accountslib.check_products_on_wishlist(wd, "Lovehoney Dream Bullet 10 Function Bullet Vibrator (Pink)", failures)
       #Delete wishlists created for test
       accountslib.delete_all_wishlists(wd, "//div[contains(@class,'wishlist')]/ol/li[1]/ul")
       accountslib.delete_all_wishlists(wd, "//div[contains(@class,'wishlist')]/ol/li[1]/ul")
       #Check wishlists are gone
       failures = accountslib.check_wishlists_are_gone(wd, "Vee", failures)
       failures = accountslib.check_wishlists_are_gone(wd, "Rockstar", failures)
       failures = accountslib.check_wishlists_are_gone(wd, "Relentless", failures)
       failures = accountslib.check_wishlists_are_gone(wd, "Redbull", failures)
       failures = accountslib.check_wishlists_are_gone(wd, "Monster", failures)
       #Clear cookies to log out ready for next test
       wd.delete_all_cookies()
   except:
       print ('fell over')
       print(traceback.print_exc())
       failures = failures + 1
   return failures

def wishlist_purchase_and_sharing_2(self, targetContainerID):
   wd = self.wd
   wd.maximize_window()
   failures = 0
   try:
        if targetContainerID == 'beta':
            accountslib.getAccountPage(wd, 'UK')
        else:
            accountslib.getAccontPageContainer(wd, 'UK', targetContainerID)
        misc.accept_cookies(wd, 'UK', 0)
        # Login to wishlist test account:
        accountslib.accountLogin(wd, "julian.bowles+totestaccount@lovehoney.co.uk", "lovehoney")
        # Go to wishlist page in test account:
        accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
        # check if any wishlists have been left by a prevoius test run that failed, added below:
        accountslib.delete_all_wishlists(wd, "//*[@id='main']/div/ol/li[1]/ul")
        #Create 3 test wishlists and add products to each of them and set privacy level
        returnArray = accountslib.makewishlist(wd, 'Only Me', failures)
        failures = returnArray[0]
        wishlist_id = returnArray[1]
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "319", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "16603", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "34938", wishlist_id)
        accountslib.set_privacy_option_on_wishlist(wd, "OM", "Edit wishlist settings")
        returnArray = accountslib.makewishlist(wd, 'Anyone who views my public profile', failures)
        failures = returnArray[0]
        wishlist_id = returnArray[1]
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "319", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "16603", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "34938", wishlist_id)
        accountslib.set_privacy_option_on_wishlist(wd, "ATVMPP", "Edit wishlist settings")
        returnArray = accountslib.makewishlist(wd, 'Only people i have shared it with', failures)
        failures = returnArray[0]
        wishlist_id = returnArray[1]
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "319", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "16603", wishlist_id)
        accountslib.add_product_to_existing_wishlist(wd, "lovehoneycouk.beta", "34938", wishlist_id)
        accountslib.set_privacy_option_on_wishlist(wd, "OPSW", "Edit wishlist settings")
        #Share one wishlist with friends
        failures = accountslib.share_wishlist_with_1_user(wd, "Share this wishlist", "1", "atester1@qa.com", "Yes", failures)
        #Check that only the wishlist shared with everyone is the only one showing up on the test users public profile
        failures = accountslib.checking_wishlist_was_shared_on_community_page(wd, "https://lovehoneycouk.beta", "3744596", failures)
        #Clearing up after testing
        accountslib.getAccountPage(wd, 'UK')
        # Login to wishlist test account:
        accountslib.accountLogin(wd, "julian.bowles+totestaccount@lovehoney.co.uk", "lovehoney")
        # Go to wishlist page in test account:
        accountslib.get_wishlist_page(wd, "https://lovehoneycouk.beta")
        # check if any wishlists have been left by a prevoius test run that failed, added below:
        accountslib.delete_all_wishlists(wd, "//*[@id='main']/div/ol/li[1]/ul")
        #Clear cookies to logout test users
        wd.delete_all_cookies()
   except:
       print ('fell over')
       print(traceback.print_exc())
       failures = failures + 1
   return failures

def check_footer(self, site, expected_links):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        failures = misc.check_and_count_links_by_tag_name(wd, site, 'a', expected_links, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def reint_containers(self, branch):
    wd = self.wd
    try:
         misc.reintandreloadsites_containers(wd, branch)
    except:
        print ('fell over')
        print(traceback.print_exc())

def country_account_set_address(self, targetContainerID):
    wd = self.wd
    accountPage = AccountPage(self.wd)
    productPage = ProductPage(self.wd)
    addressBookPage = AccountAddressBookPage(self.wd)
    failures = 0
    try:
        if targetContainerID == 'beta':
            accountPage.getAccountPage("UK", targetContainerID)
        else:
            accountPage.getAccountPageContainer('UK', targetContainerID)

        accountPage.login("julian.bowles+totestaccount@lovehoney.co.uk", "lovehoney")
        productPage.accept_gdpr_cookies()
        accountPage.openAddressBook("UK")
        addressBookPage.addAddress()
        addressBookPage.expandAddressField("UK")
        failures = misc.checkCountrySaved(wd, failures) # This loops through and adds all countries
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures+1
    return failures

def Braintree_Suspend_reason_tests_opc(self, targetContainerID, site, paytype, cardnumber, CV2, prod, email, addlineone, city, postcode, loyalty, login, suspend, fraud):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    confirm = ConfirmationPage(self.wd)
    opc = OnePageCheckout(self.wd)
    wd.maximize_window()
    failures = 0
    print ('-'*20)
    # Make a random email address to use for order that should not be suspended
    if email == 'random':
        print("Making a random email address")
        ramdomemail = misc.random_email()
    else:
        print("Using an email address specified")
    try:
        print ("Card number = {0} Site = {1} CVV = {2}".format(cardnumber, site, CV2))

        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.clearCountrySwitchBanner()
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        time.sleep(2)
        basketPage.proceedToOPC(paytype)
        #Do we need a random email address
        if email == 'random':
            opc.loginBeforeCheckout(ramdomemail, 'autotester!!', login, paytype)
        else:
            opc.loginBeforeCheckout(email, 'autotester!!', login, paytype)
        #Checking out
        opc.runCheckoutOPC_any_add(paytype, site, cardnumber, CV2, email, addlineone, city, postcode, loyalty, login)
        #Going to the db to get the things we need
        ordernumber = confirm.returnOrderNoOPC(site)
        order_status = sql.getOrderStatus(ordernumber)
        cv2_message = sql.getPaymenttext(ordernumber)
        suspend_reasons = sql.getSuspendreason(ordernumber)
        failures = misc.check_if_order_suspended_opc(order_status, suspend, email, suspend_reasons, failures)
        failures = misc.check_order_payment_text_opc(CV2, cv2_message, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def jibber_black_box_kick(self, env):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    jibber = Jibber(self.wd)
    try:
        jibber.getJibber(env)
        if env == "ci":
            jibber.login("love.honey", "lovehoney")
        else:
            jibber.login("qa.autotest", "trying2FIX!")
        jibber.get_black_box_page(env, failures)
        jibber.click_the_kicker(failures)
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_opc_login_modal_guest(self, targetContainerID, site, prod, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='opc_modal_checks_1')
        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        time.sleep(2)
        basketPage.proceedToOPC(paytype)
        #Checking the modal as it shows after proceeding from basket
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'First_open_modal')
        #Add email address
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        #Enter an email address
        #Checking the modal as it shows after adding email address
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email entered')
        #Remove email address
        opc.remove_email_address('30')
        #Checking the modal as it shows after clearing email address
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email deleted')
        #Clicking the proceed button to make sure it does not let you thru
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see it didn't allow the users to continue
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email deleted checking you cant continue')
        #Putting in an invalid email address and trying to submit
        opc.just_add_email("iwantmytoy.com")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see if the messages look right
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address has provoked correct message')
        #Deleting the bad email address
        opc.remove_email_address('14')
        #Checking the modal to see if the validation messages for bad email address are gone
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address message has been cleared')
        #Adding a correct email address and checking we make it to the OPC
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        productPage.clickElementById("checkoutLoginSubmit")
        productPage.waitForId("shippingOption1")
        #Checking we make it to OPC
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address message has been cleared')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_opc_login_modal_login(self, targetContainerID, site, prod, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='opc_modal_checks_login_2')
        print('Open issue - SHIELD-619 - Erroneous blue text in OPC error modal')
        #Get to the opc login modal
        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        basketPage.proceedToOPC(paytype)
        #selecting the login radio button on the login modal
        opc.clickElementByCss(opc.loginRadio)
        #Checking the modal after selecting the login with account option
        misc.eyes_check_window_fluid(eyes, site, 'Modal after login with account selected')
        #Adding an email address to the modal
        opc.clickElementById("checkoutLoginLoginTrue")
        opc.just_add_email("auto.testercouk@lovehoney.co.uk")
        #Checking the modal after selecting the login with account option
        misc.eyes_check_window_fluid(eyes, site, 'Modal after login with account selected and email address added')
        #Delete the email address
        opc.remove_email_address('31')
        # #Entering an invalid email address
        opc.clickElementById("checkoutLoginLoginFalse")
        opc.just_add_email("iwantmytoy.com")
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have tried to continue with invalid email address
        misc.eyes_check_window_fluid(eyes, site, 'Modal after trying to proceed with an invalid email address')
        #Delete the email address
        opc.remove_email_address('15')
        #Checking the modal after we have deleted the email address
        misc.eyes_check_window_fluid(eyes, site, 'Modal after trying deleting an invalid email address')
        #Enter a password in the password field
        opc.clickElementById("checkoutLoginLoginTrue")
        opc.sendTextByCss(opc.passwordField, "autotester!!")
        #Checking the modal after we have entered only the password
        misc.eyes_check_window_fluid(eyes, site, 'Modal after only entering a password')
        #Adding an email address and an incorrect password
        opc.just_add_email("auto.testercouk@lovehoney.co.uk")
        opc.remove_password('15')
        opc.sendTextByCss(opc.passwordField, "password")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account

        misc.eyes_check_window_fluid(eyes, site, 'Modal after only entering an email address and a password that is not correct for that account and hitting submit')
        #Clicking continue to check the bad password is correctlt dealt with Part 1
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 1
        misc.eyes_check_window_fluid(eyes, site, 'Modal after entering an email address and an incorrect account password then hitting submit')
        #click the Back to login link
        productPage.waitForIdClickable("backToLogin")
        productPage.clickElementById("backToLogin")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed
        misc.eyes_check_window_fluid(eyes, site,'Modal after clicking the back to login button having got your password wrong')
        #Clicking continue to check the bad passowrd is correctly dealt with Part 2
        opc.clickElementByCss(opc.loginRadio)
        # opc.clickElementById()
        opc.sendTextByCss(opc.passwordField, "password")
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        #Clicking the X to close the error modal
        time.sleep(3)
        productPage.clickElementById("modalClose")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 2
        misc.eyes_check_window_fluid(eyes, site,'Modal after clicking the X button on the error modal')
        # Clicking continue to check the bad passowrd is correctly dealt with Part 3
        opc.clickElementByCss(opc.loginRadio)
        opc.sendTextByCss(opc.passwordField, "password")
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        #Clicking outside the modal
        time.sleep(3)
        misc.click_out_side_element(wd, "modalClose")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 3
        misc.eyes_check_window_fluid(eyes, site, 'Modal after clicking outside the error modal')
        #Add email address
        opc.remove_email_address('31')
        opc.just_add_email("auto.testercouk@lovehoney.co.uk")
        #Checking the modal as it shows after adding email address
        misc.eyes_check_window_fluid(eyes, site, 'After email entered')
        #Remove email address
        opc.remove_email_address('30')
        #Checking the modal as it shows after clearing email address
        misc.eyes_check_window_fluid(eyes, site, 'After email deleted checking you cant continue')
        #Putting in an invalid email address and trying to submit
        opc.just_add_email("iwantmytoy.com")
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see if the messages look right
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address has provoked correct message')
        #Deleting the bad email address
        opc.remove_email_address('15')
        #Checking the modal to see if the validation messages for bad email address are gone
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address message has been cleared')
        #Adding a correct email address and checking we make it to the OPC
        opc.just_add_email("auto.testercouk@lovehoney.co.uk")
        opc.clickElementByCss(opc.loginRadio)
        opc.sendTextByCss(opc.passwordField, "autotester!!")
        productPage.waitForIdClickable("checkoutLoginSubmit")
        productPage.clickElementById("checkoutLoginSubmit")
        # productPage.waitForId("shippingOption1")
        #Checking we make it to OPC
        misc.eyes_check_window_fluid(eyes, site, 'Checking we got to OPC')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_opc_login_from_header(self, targetContainerID, site, prod, paytype, env, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    accountPage = AccountPage(self.wd)
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='opc_login_from_header_checks')

        #Logging in via the accounts page
        if targetContainerID == 'beta':
            accountPage.getAccountPage("UK", targetContainerID)
        else:
            accountPage.getAccountPageContainer("UK", targetContainerID)

        accountPage.login("auto.testercouk@lovehoney.co.uk", "autotester!!")
        #Get to the opc
        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        time.sleep(2)
        basketPage.proceedToOPC(paytype)
        #Checking there is no modal as you are aready logged in
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Straight into OPC as already logged in')
        # Complete the checkout process otherwise the basket total will increase and tests will always find differences
        opc.runCheckoutOPC(site, 'LIN', 'NON', 'CC', '5555555555554444', '150', 'auto.testercouk@lovehoney.co.uk', 'na')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def test_opc_login_modal_forgot_PW(self, targetContainerID, site, prod, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='opc_login_forgot_password')
        #Get to the opc login modal
        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        time.sleep(2)
        basketPage.proceedToOPC(paytype)
        #Selecting the login radio button on the login modal
        opc.clickElementByCss(opc.loginRadio)
        #Clicking the forgot password link
        productPage.clickElementById("checkoutLoginForgotPassword")
        #Checking the forgot password modal is shown
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Forgot password page')
        #Go back to modal and select login radio
        productPage.browserBack()
        productPage.refreshpage()
        # wd.waitForXpath('//*[@id="reload-button"]')
        # wd.execute_script("document.querySelector('#reload-button').click()")
        opc.waitForCss(opc.loginRadio)
        opc.clickElementByCss(opc.loginRadio)
        # productPage.refreshpage()
        #Put in email address but bad password
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        opc.sendTextByCss(opc.passwordField, "password")
        productPage.clickElementById("checkoutLoginSubmit")
        #Check correct error messages are shown
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Bad password errors')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_opc_login_closing(self, targetContainerID, site, prod, paytype, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    opc = OnePageCheckout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='opc_login_modal_closing')
        #Get to the opc login modal
        productPage.getSelectedProductPage(site, prod, targetContainerID)
        productPage.accept_gdpr_cookies()
        productPage.addProductToBasket()
        interstitialPage.proceedToBasket()
        basketPage.selectCountry(site)
        time.sleep(2)
        basketPage.proceedToOPC(paytype)
        #Checking the modal, it should not have a X to close it shown
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Guest Modal:Check modal for no cross')
        #Checking we can click around the modal to exit it
        misc.click_out_side_element(wd, "checkoutLoginEmail")
        #Checking the modal has not been dismissed by clicking around it
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Guest Modal:Check modal still there after clicking around it 1')


        #Click the radio button for already have an account
        opc.clickElementByCss(opc.loginRadio)
        #Sending the escape key
        misc.send_escape_key(wd)
        #Checking the modal has not been dismissed by hitting the esc key
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Login Modal:Check modal still there after escape key pressed 2')
        ##Going back to basket page
        productPage.browserBack()
        # Checking the user is taken back to the subs landing page
        time.sleep(10)
        misc.eyes_check_window_fluid(eyes, site, 'Login Modal:Check user taken back to basket page')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def checkout_subs(self):
    wd = self.wd
    productPage = ProductPage(self.wd)
    interstitialPage = InterstitialPage(self.wd)
    basketPage = BasketPage(self.wd)
    confirm = ConfirmationPage(self.wd)
    opc = OnePageCheckout(self.wd)
    wd.maximize_window()
    failures = 0
    try:
        email = misc.getEmail('UK')
        opc.getSubsCheckout()
        opc.loginBeforeCheckout(email, 'autotester!!', 'LIN', 'CC')
        opc.runCheckoutOPC('UK', 'LIN', 'NON', 'CC', '4012000033330026', '150', email, 'na')

    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_subs_login_modal_guest(self, targetContainerID, site, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    opc = OnePageCheckout(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='Subs_modal_checks_1')
        print('Open issue - SUBS-445 - Divider appears below content on Subs Checkout page')
        #Get to the subs login modal
        subs.getSubsCheckout(targetContainerID)
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        # Clicking the subscribe button
        subs.clickElementByCss(subs.subscribe_button)
        #Checking the modal as it shows after proceeding from basket
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'First_open_modal')
        #Add email address
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        #Enter an email address
        #Checking the modal as it shows after adding email address
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email entered')
        #Remove email address
        opc.remove_email_address('30')
        #Checking the modal as it shows after clearing email address
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email deleted')
        #Clicking the proceed button to make sure it does not let you thru
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see it didn't allow the users to continue
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'After email deleted checking you cant continue')
        #Putting in an invalid email address and trying to submit
        opc.just_add_email("iwantmytoy.com")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see if the messages look right
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address has provoked correct message')
        #Deleting the bad email address
        opc.remove_email_address('14')
        #Checking the modal to see if the validation messages for bad email address are gone
        time.sleep(2)
        misc.eyes_check_window_fluid(eyes, site, 'Checking invalid email address message has been cleared')
        #Adding a correct email address and checking we make it to the OPC
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        productPage.clickElementById("checkoutLoginSubmit")
        productPage.waitForId("deliveryaddress-lookup__enable-manual-input")
        #Checking we make it to OPC
        misc.eyes_check_window_fluid(eyes, site, 'Checking correct email address gets you into the checkout')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_subs_login_modal_login(self, targetContainerID, site, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    opc = OnePageCheckout(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='subs_modal_checks_login_2')
        # Get to the subs login modal
        subs.getSubsCheckout(targetContainerID)
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        # Clicking the subscribe button
        subs.clickElementByCss(subs.subscribe_button)
        #selecting the login radio button on the login modal
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        #Checking the modal after selecting the login with account option
        time.sleep(2)
        #misc.eyes_check_window(eyes, site, 'Modal after login with account selected')
        #Adding an email address to the modal
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        #Checking the modal after selecting the login with account option
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after login with account selected and email address added')
        #Clicking the continue without the password field being set
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after selecting the proceed to payment button with no password set
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after clicking the proceed button with not password entered')
        #Delete the email address
        opc.remove_email_address('30')
        #Checking the modal after deleting the email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after email address has been deleted')
        #Clicking the proceed button
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have tried to continue with no email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after email address has been deleted and we clicked proceed')
        #Entering an invalid email address
        opc.just_add_email("iwantmytoy.com")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have tried to continue with invalid email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after trying to proceed with an invalid email address')
        #Delete the email address
        opc.remove_email_address('14')
        #Checking the modal after we have deleted the email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after trying deleting an invalid email address')
        #Enter a password in the password field
        opc.sendTextById(opc.passwordFieldID, "cartman")
        #Checking the modal after we have entered only the password
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after only entering a password')
        #Clicking continue when we only have a password entered
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have entered only the password and hit proceed
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after only entering a password and hitting submit')
        #Adding an email address and an incorrect password
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        opc.remove_password('14')
        opc.sendTextById(opc.passwordFieldID, "password")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after only entering an email address and a password that is not correct for that account and hitting submit')
        #Clicking continue to check the bad password is correctlt dealt with Part 1
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 1
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Modal after entering an email address and an incorrect account password then hitting submit')
        #click the Back to login link
        productPage.clickElementById("backToLogin")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed
        time.sleep(2)
        misc.eyes_check_window(eyes, site,'Modal after clicking the back to login button having got your password wrong')
        #Clicking continue to check the bad passowrd is correctly dealt with Part 2
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        opc.sendTextById(opc.passwordFieldID, "password")
        productPage.clickElementById("checkoutLoginSubmit")

        # ------------------------------------------------------------------------------------#

        #This bit was Clicking the X to close the error modal BUT after a code change clicking the X is not longer possible with selenium so going back via the back to login link
        #productPage.clickElementById("modalClose")
        productPage.clickElementById("backToLogin")
        # This bit was checking the modal after Clicking the X to close the error modal BUT after a code change clicking the X is not longer possible with selenium so going back via the back to login link
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 2
        #time.sleep(2)
        #misc.eyes_check_window(eyes, site,'Modal after clicking the X button on the error modal')

        # ------------------------------------------------------------------------------------#

        # Clicking continue to check the bad passowrd is correctly dealt with Part 3
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        opc.sendTextById(opc.passwordFieldID, "password")
        productPage.clickElementById("checkoutLoginSubmit")

        # ------------------------------------------------------------------------------------#

        #This bit was clicking outside the modal to check this worked BUT after a code change clicking outside the modal is not longer possible
        #misc.click_out_side_element(wd, "modalClose")
        #Checking the modal after we have entered both a valid email address and a password that is not right for that account and hit proceed Part 3
        #time.sleep(2)
        #misc.eyes_check_window(eyes, site, 'Modal after clicking outside the error modal')

        # ------------------------------------------------------------------------------------#

        productPage.clickElementById("backToLogin")

        #Add email address
        opc.remove_email_address('30')
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        #Checking the modal as it shows after adding email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'After email entered')
        #Remove email address
        opc.remove_email_address('30')
        #Checking the modal as it shows after clearing email address
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'After email deleted')
        #Clicking the proceed button to make sure it does not let you thru
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see it didn't allow the users to continue
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'After email deleted checking you cant continue')
        #Putting in an invalid email address and trying to submit
        opc.just_add_email("iwantmytoy.com")
        productPage.clickElementById("checkoutLoginSubmit")
        #Checking the modal to see if the messages look right
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Checking invalid email address has provoked correct message')
        #Deleting the bad email address
        opc.remove_email_address('14')
        #Checking the modal to see if the validation messages for bad email address are gone
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Checking invalid email address message has been cleared')
        #Adding a correct email address and checking we make it to the OPC
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        opc.sendTextById(opc.passwordFieldID, "cartman")
        productPage.clickElementById("checkoutLoginSubmit")
        productPage.waitForId("deliveryChangeSelected")
        #Checking we make it to OPC
        misc.eyes_check_window(eyes, site, 'Checking we got into checkout')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_subs_login_from_header(self, targetContainerID, site, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    accountPage = AccountPage(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='subs_login_from_header_checks')
        #Logging in via the accounts page
        if targetContainerID == 'beta':
            accountPage.getAccountPage("UK", targetContainerID)
        else:
            accountPage.getAccountPageContainer(site, targetContainerID)
        accountPage.login("julian.bowles@lovehoney.co.uk", "cartman")
        # Get to the subs login modal
        subs.getSubsCheckout(targetContainerID)
        # subs.getSubsCheckout("2285")
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        # Clicking the subscribe button
        subs.clickElementByCss(subs.subscribe_button)
        #Checking there is no modal as you are aready logged in
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Straight into subs checkout as already logged in')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def test_subs_login_modal_forgot_PW(self, targetContainerID, site, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    opc = OnePageCheckout(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='subs_login_forgot_password')
        # Get to the subs login modal
        subs.getSubsCheckout(targetContainerID)
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        # Clicking the subscribe button
        subs.clickElementByCss(subs.subscribe_button)
        #Selecting the login radio button on the login modal
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        #Clicking the forgot password link
        productPage.clickElementById("checkoutLoginForgotPassword")
        #Checking the forgot password modal is shown
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Forgot password page')
        #Go back to modal and select login radio
        productPage.browserBack()
        subs.clickElementById(subs.Login_before_subs_check_out_radio)
        #Put in email address but bad password
        opc.just_add_email("julian.bowles@lovehoney.co.uk")
        opc.sendTextById(opc.passwordFieldID, "password")
        productPage.clickElementById("checkoutLoginSubmit")
        #Check correct error messages are shown
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Bad password errors')
        #Clicking the rest password link
        time.sleep(2)
        productPage.clickElementByLinkText("Reset your password")
        #Check reset password screen is shown
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Bad password errors')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_subs_login_closing(self, targetContainerID, site, eyes):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    productPage = ProductPage(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Opening eyes ready to check modal
        eyes.open(driver=wd, app_name='Applitools', test_name='subs_login_modal_closing')
        # Get to the subs login modal
        subs.getSubsCheckout(targetContainerID)
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        # Clicking the subscribe button
        subs.clickElementByCss(subs.subscribe_button)
        #Checking the modal, it should not have a X to close it shown
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Check modal for no cross 1')
        #Checking we can click around the modal to exit it
        misc.click_out_side_element(wd, "checkoutLoginEmail")
        #Checking the modal has not been dismissed by clicking around it
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Check modal still there after clicking around it 1')
        #Sending the escape key
        misc.send_escape_key(wd)
        #Checking the modal has not been dismissed by hitting the esc key
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Check modal still there after escape key pressed')
        #Checking the user is taken back to the basket page
        productPage.browserBack()
        # Checking the user is taken back to the subs splash screen
        time.sleep(2)
        misc.eyes_check_window(eyes, site, 'Check user taken back to subs splash screen')
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def winnow_user_journer_react_1(self, eyes, URL):
    wd = self.wd
    #wd.maximize_window()
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    #URL strings to use
    sextoys = "sex-toys/"
    vibrators = "vibrators/"
    pricerange = "?price=50-100"
    #Selectors for filters
    silicone = "[data-key='Silicone']"
    ABS = "[data-key='ABS']"
    fiftytoonehundred = "[data-key='50-100']"
    insertablelength5to6 = "[data-key='5-6']"
    brand = "[data-key='Lovehoney Desire']"
    #Xpaths
    sextoysbreadcrumb = "//*[@id='search-winnow']/div/div[2]/div/ul/li[2]/a"

    try:
        #Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='winnow user journey 1')
        #Turn on the winnow switch
        # win.flickWinnowSwitch(env)
        #Get the starting page for the winnow tests
        win.get(URL+sextoys)
        misc.accept_cookies(wd, "UK", failures)
        #Checking the page
        misc.eyes_check_window_fluid(eyes, "UK", "Sex Toys group page")
        #Checking the home page breadcrumb link
        Page.clickElementByLinkText("Home")
        #Checking we get the homepage when we click on the breadcrumb for homepage
        failures = val.check_currenturl_with_given_url(wd, URL, "Homepage", failures)
        Page.browserBack()
        #Checking when we go back to get the winnow page we expect
        failures = val.check_currenturl_with_given_url(wd, URL+sextoys, "Uber group sex toys", failures)
        #Checking a group
        Page.clickElementByLinkText("Vibrators")
        #Checking the page but waiting for an element as the page loads slowly
        Page.waitForCss(fiftytoonehundred)
        #Checking the breadcrumb for sex toys
        Page.clickElementByXpath(sextoysbreadcrumb)
        #Checking we get the Sex toys group when we click on the breadcrumb for Sex toys
        failures = val.check_currenturl_with_given_url(wd, URL+sextoys, "Sex Toys", failures)
        Page.browserBack()
        #Checking when we go back to get the winnow page we expect
        failures = val.check_currenturl_with_given_url(wd, URL+sextoys+vibrators, "Vibrators group", failures)
        #Checking the home page breadcrumb link
        Page.clickElementByLinkText("Home")
        #Checking we get the Sex toys group when we click on the breadcrumb for home page
        failures = val.check_currenturl_with_given_url(wd, URL, "Homepage", failures)
        Page.browserBack()
        #Checking when we go back to get the winnow page we expect
        failures = val.check_currenturl_with_given_url(wd, URL+sextoys+vibrators, "Vibrators group", failures)
        misc.eyes_check_window_fluid(eyes, "UK", "Vibrators group page")
        Page.clickElementByCss(fiftytoonehundred)
        #Appling multiple filters testing = Price range £50 - £100, Material Silicone, Insertable length  7-8 inches, Brand fun factory.
        failures = val.check_currenturl_with_given_url(wd, URL+sextoys+vibrators+pricerange, "£50 - £100 selected", failures)
        #Selecting the drop down Material silicone, ugly sleep due to having to wait for filter to load after a filter has been selected
        time.sleep(2)
        Page.waitForCss(silicone)
        Page.clickElementByCss(silicone)
        #Selecting insertable length, ugly sleep due to having to wait for filter to load after a filter has been selected
        # Page.waitForCss(insertablelength5to6)
        # Page.clickElementByCss(insertablelength5to6)
        #Selecting brand, ugly sleep due to having to wait for filter to load after a filter has been selected
        Page.waitForCss(brand)
        Page.clickElementByCss(brand)
        #Checking the final winnowed selection
        misc.eyes_check_window_fluid(eyes, "UK", "Final winnowed selection")
        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def winnow_user_journer_react_2(self, eyes, URL):
    wd = self.wd
    #wd.maximize_window()
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    #URL strings to use
    sextoys = "sex-toys/"
    # Selectors for filters
    rating4andup = "?averageRating=1+&p=1"
    onestarandup = ".rating-list-item"
    fiftytoonehundred = "[data-key='£50 - £100']"
    insertablelength7to8 = "[data-key='7 to 8 inches']"
    branddropdown = ".filter--brand select"
    brand = "Fun Factory"
    material = "Silicone"

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='winnow user journey 2')
        # Turn on the winnow switch
        # win.flickWinnowSwitch("-1612.container.ci")
        # Get the starting page for the winnow tests
        win.get(URL + sextoys)
        misc.accept_cookies(wd, "UK", failures)
        #Step 10 in https://lovehoney2.testrail.net/index.php?/cases/view/324343
        Page.clickElementByLinkText("Vibrators")
        #Click the non cat filters one by one and then hit back button to check the order and filter is maintained from the page before the back
        # Clicking the rating 1 stars and up filter
        Page.waitForCss(onestarandup)
        Page.clickElementByCss(onestarandup)
        Page.waitForCss(".reset-filters")
        Page.browserBack()
        Page.waitForCss(onestarandup)
        misc.eyes_check_window_fluid(eyes, "UK", "Browser back after one star and up (First filter) ratings change filter was applied")
        # Clicking the price range filter
        Page.clickElementByCss(fiftytoonehundred)
        Page.waitForCss(".reset-filters")
        Page.browserBack()
        Page.waitForCss(onestarandup)
        misc.eyes_check_window_fluid(eyes, "UK", "Browser back after Price change filter was applied")
        # Selecting the material
        Page.selectDropdownCss("select", material)
        Page.waitForCss(".reset-filters")
        Page.browserBack()
        Page.waitForCss(onestarandup)
        misc.eyes_check_window_fluid(eyes, "UK", "Browser back after material filter was applied")
        # Selecting the Insertable length
        Page.waitForCss(insertablelength7to8)
        Page.clickElementByCss(insertablelength7to8)
        Page.waitForCss(".reset-filters")
        Page.browserBack()
        Page.waitForCss(onestarandup)
        misc.eyes_check_window_fluid(eyes, "UK", "Browser back after insertable length filter was applied")
        # Selecting the Brand
        Page.selectDropdownCss(branddropdown, brand)
        Page.waitForCss(".reset-filters")
        Page.browserBack()
        Page.waitForCss(onestarandup)
        misc.eyes_check_window_fluid(eyes, "UK", "Browser back after brand filter was applied")

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


''' This function has been written to work with the second version of React Winnow which contains
new class and data-key DOM elements which are used to access filter options '''
def winnow_clear_filters_v2_1(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    # URL strings to use
    sexyLingerie = 'sexy-lingerie/'
    basquesCorests = 'lingerie-sets/basques-corsets-bustiers/'

    print('Test 1')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowV2:Clear All Filters')
        # Get and check the Uber page
        win.get(URL + sexyLingerie)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Ubergroup: ' + sexyLingerie)
        misc.eyes_check_window(eyes, "UK ", sexyLingerie)

        # Get and check the Category page
        win.get(URL + sexyLingerie + basquesCorests)
        print('Loading Page: ' + sexyLingerie + basquesCorests)
        misc.eyes_check_window_fluid(eyes, "UK ", sexyLingerie+basquesCorests)

        win.selectWinnowFacetOption('category', 'baby-dolls-chemises')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Cat: Baby Dolls & Chemises')

        win.selectWinnowFacetOption('facet-size', '4')
        win.selectWinnowFacetOption('facet-size', '6')
        win.selectWinnowFacetOption('facet-size', '24')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Size: 4, 6 & 24')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        print('Finished Test 1')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with the second version of React Winnow which contains
new class and data-key DOM elements which are used to access filter options '''
def winnow_clear_filters_v2_2(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    # URL strings to use
    loadPage = 'sexy-lingerie/lingerie-sets/'

    print('Test 2')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowV2:Clear All Filters 2')
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('category','bodies-teddies')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Cat: Bodies & Teddies')

        win.selectWinnowViewAll('facet-size')
        misc.eyes_check_window_fluid(eyes, "UK ", 'View All Sizes')

        win.selectWinnowFacetOption('facet-size','10')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Size: 10')

        win.selectWinnowFacetOption('brand','Lovehoney Lingerie')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Brand: Lovehoney Lingerie')

        win.selectWinnowFacetOption('colour','Black')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Colour: Black')

        win.selectWinnowFacetOption('price','10-20')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Price: Under £25')

        win.selectWinnowFacetOption('material','Lace')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Material: Lace')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        print('Finished Test 2')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with the second version of React Winnow which contains
new class and data-key DOM elements which are used to access filter options '''
def winnow_clear_filters_v2_3(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)

    print('Test 3')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowV2:Clear All Filters 3')
        loadPage = 'sex-toys/vibrators/'
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('material','ABS Plastic')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Material: ABS')

        # win.selectWinnowFacetOption('length', '5-6')
        # misc.eyes_check_window(eyes, "UK ", '+Inset Len: 5 to 6 inches')

        win.selectWinnowViewAll('brand')
        misc.eyes_check_window_fluid(eyes, "UK ", 'View All Brands')

        win.selectWinnowFacetOption('brand', 'Lovehoney')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Brand: Lovehoney')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        loadPage = 'sex/lubricants/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('volume', '0-100')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Volume: Up to 100ml')

        win.selectWinnowFacetOption('lube-type', 'Water based')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Lube type: Water based')

        win.selectWinnowFacetOption('lube-function', 'Sex and sex toys')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Lube funct: Sex and sex toys')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        print('Finished Test 3')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with the second version of React Winnow which contains
new class and data-key DOM elements which are used to access filter options '''
def winnow_clear_filters_v2_4(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)

    print('Test 4')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowV2:Clear All Filters 4')
        loadPage = 'sex/condoms/'
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('condom-size', 'Large')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Condom Size: Large')

        win.selectWinnowFacetOption('lubricated', 'Yes, non-spermicidal')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Lubricated: Yes, non-spermicidal')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        loadPage = 'bondage/handcuffs-restraints/bedroom-bondage-kits/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('fastening', 'Velcro')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Fastening: Velcro')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        loadPage = 'sex/better-sex-for-her/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        # win.selectWinnowFacetOption('how-fast', 'Fast acting')  # Confirm category name and selector, this element isn't currently displayed
        # # misc.eyes_check_window(eyes, "UK ", 'How fast: Fast Acting')
        #
        # win.selectWinnowFacetOption('what-for', 'Stimulator')  # Confirm category name and selector, this element isn't currently displayed
        # # misc.eyes_check_window(eyes, "UK ", '+Whats it for: Stimulator')

        win.selectWinnowFacetOption('pharmacy', 'Cream')
        misc.eyes_check_window_fluid(eyes, "UK ", '+Pharmacy drug type: Cream')

        # win.winnowClearAllClick() # Clear all will be needed when 'Fast acting' and/or 'what-for' apear as filter options
        # # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        loadPage = 'sex-toys/male-sex-toys/fleshlights/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.selectWinnowFacetOption('opening', 'Butt')
        misc.eyes_check_window_fluid(eyes, "UK ", 'Opening: Butt')

        win.winnowClearAllClick()
        misc.eyes_check_window_fluid(eyes, "UK ", 'Clear All Filters')

        print('Finished Test 4')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def test_checkout_subs(self, site, login, email, paytype, pw, container):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    opc = OnePageCheckout(self.wd)
    subs = Subs_checkout(self.wd)
    try:
        #Getting the landing page
        subs.getSubsCheckout(container)
        # Getting rid of the cookies banner
        misc.accept_cookies(wd, "UK", failures)
        #Hitting the subscribe button
        subs.waitForXpathClickable("//a[contains(@class, 'subscription-landing__cta-button')]")
        subs.clickElementByXpath("//a[contains(@class, 'subscription-landing__cta-button')]")
        #Doing subs modal
        subs.loginBeforeSubsCheckout(email, pw, login, paytype)
        #Adding name
        subs.addName(login, "Julian", "Bowles")
        #Putting in card details
        opc.runCheckoutOPC_applitoolsSubs(site, login, 'NON', paytype, '4012000033330026', '150', email)
        #Submitting order
        opc.submit_order_OPC()
        # time.sleep(10)
        opc.waitForXpathClickable("//a[contains(@class,'message__homepage-link')]")
        # opc.waitForCss('.message__heading')
        #Checking confirmation page is shown
        confirmation_page_text = wd.find_element_by_css_selector('.message__heading').text
        opc.check_text(confirmation_page_text, "Thank you!", failures)
    except:
        failures = failures + 1
        print(traceback.print_exc())
        print('fell over')
        shotname = misc.name_the_screenshot_by_site(site)
        wd.save_screenshot('test-reports/subs_uk.png'.format(shotname))
        print('-' * 20)
    return failures

''' This function has been written to work with React Winnow MOBILE'''
def winnow_clear_filters_Mobile_v2_1(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    # URL strings to use
    lingerieSets = 'sexy-lingerie/lingerie-sets/'

    print('Test 1')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowMobile-1:Clear All Filters')
        # Get and check the Uber page
        win.get(URL + lingerieSets)
        misc.accept_cookies(wd, "UK", failures)
        misc.eyes_check_window_fluid(eyes, "UK ", 'Lingerie Sets')

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('category')
        win.selectWinnowFacetOption('category', 'baby-dolls-chemises')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Cat: Baby Dolls & Chemises')
        # misc.eyes_check_window(eyes, "UK ", 'Cat: Baby Dolls & Chemises')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('facet-size')
        win.selectWinnowFacetOption('facet-size', '4')
        win.selectWinnowFacetOption('facet-size', '6')
        win.selectWinnowFacetOption('facet-size', '24')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Size: 4, 6 & 24')
        # misc.eyes_check_window(eyes, "UK ", '+Size: 4, 6 & 24')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        print('Finished Test 1')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with React Winnow MOBILE'''
def winnow_clear_filters_Mobile_v2_2(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)
    # URL strings to use
    loadPage = 'sexy-lingerie/lingerie-sets/'

    print('Test 2')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowMobile-2:Clear All Filters')
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('category')
        win.selectWinnowFacetOption('category','bodies-teddies')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Cat: Bodies & Teddies')
        # misc.eyes_check_window(eyes, "UK ", 'Cat: Bodies & Teddies')
        win.toggleWinnowFacetOption('category')

        win.toggleWinnowFacetOption('facet-size')
        win.selectWinnowFacetOption('facet-size','10')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Size: 10')
        # misc.eyes_check_window(eyes, "UK ", '+Size: 10')
        win.toggleWinnowFacetOption('facet-size')

        win.toggleWinnowFacetOption('brand')
        win.selectWinnowFacetOption('brand','Lovehoney Lingerie')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Brand: Lovehoney Lingerie')
        # misc.eyes_check_window(eyes, "UK ", '+Brand: Lovehoney Lingerie')
        win.toggleWinnowFacetOption('brand')

        win.toggleWinnowFacetOption('colour')
        win.selectWinnowFacetOption('colour','Black')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Colour: Black')
        # misc.eyes_check_window(eyes, "UK ", '+Colour: Black')
        win.toggleWinnowFacetOption('colour')

        win.toggleWinnowFacetOption('price')
        win.selectWinnowFacetOption('price','10-20')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Price: £0 - £25')
        # misc.eyes_check_window(eyes, "UK ", '+Price: £0 - £25')

        win.toggleWinnowFacetOption('material')
        win.selectWinnowFacetOption('material','Lace')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Material: Lace')
        # misc.eyes_check_window(eyes, "UK ", '+Material: Lace')
        win.toggleWinnowFacetOption('material')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        print('Finished Test 2')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with React Winnow MOBILE'''
def winnow_clear_filters_Mobile_v2_3(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)

    print('Test 3')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='WinnowMobile-3:Clear All Filters')
        loadPage = 'sex-toys/vibrators/'
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('material')
        win.selectWinnowFacetOption('material','ABS Plastic')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Material: ABS')
        # misc.eyes_check_window(eyes, "UK ", 'Material: ABS')
        win.toggleWinnowFacetOption('material')

        win.toggleWinnowFacetOption('length')
        win.selectWinnowFacetOption('length', '0-5')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Inset Len: 5 to 6 inches')
        # misc.eyes_check_window(eyes, "UK ", '+Inset Len: 5 to 6 inches')
        win.toggleWinnowFacetOption('length')

        win.toggleWinnowFacetOption('brand')
        win.selectWinnowFacetOption('brand', 'Lovehoney')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Brand: Positive Vibes')
        # misc.eyes_check_window(eyes, "UK ", '+Brand: Positive Vibes')
        win.toggleWinnowFacetOption('brand')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        win.clickMobileViewProducts()
        # misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        misc.eyes_check_window_fluid(eyes, "UK ", '|View All Products')

        loadPage = 'sex/lubricants/'
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('volume')
        win.selectWinnowFacetOption('volume', '0-100')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Volume: Up to 100ml')
        win.toggleWinnowFacetOption('volume')


        win.toggleWinnowFacetOption('lube-type')
        win.selectWinnowFacetOption('lube-type', 'Water based')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Lube type: Cream')
        # misc.eyes_check_window(eyes, "UK ", '+Lube type: Cream')
        win.toggleWinnowFacetOption('lube-type')


        win.toggleWinnowFacetOption('lube-function')
        win.selectWinnowFacetOption('lube-function', 'Oral sex')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Lube funct: Sex and sex toys')
        # misc.eyes_check_window(eyes, "UK ", '+Lube funct: Sex and sex toys')
        win.toggleWinnowFacetOption('lube-function')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        print('Finished Test 3')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

''' This function has been written to work with React Winnow MOBILE'''
def winnow_clear_filters_Mobile_v2_4(self, eyes, URL):
    wd = self.wd
    failures = 0
    Page = ProductPage(self.wd)
    win = Winnow(self.wd)

    print('Test 4')

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='MobileWinnow-4:Clear All Filters')
        loadPage = 'sex/condoms/'
        win.get(URL + loadPage)
        misc.accept_cookies(wd, "UK", failures)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('condom-size')
        win.selectWinnowFacetOption('condom-size', 'Large')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Condom Size: Large')
        # misc.eyes_check_window(eyes, "UK ", 'Condom Size: Large')
        win.toggleWinnowFacetOption('condom-size')


        win.toggleWinnowFacetOption('lubricated')
        win.selectWinnowFacetOption('lubricated', 'Yes, non-spermicidal')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Lubricated: Yes, non-spermicidal')
        # misc.eyes_check_window(eyes, "UK ", 'Lubricated: Yes, non-spermicidal')
        win.toggleWinnowFacetOption('lubricated')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')


        loadPage = 'bondage/handcuffs-restraints/bedroom-bondage-kits/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        ''' The 'Category' menu element will be open at thsi point.  If it isn't closed with the following function then 
        Selenium isn't able to see the 'Fastenings' facet.'''
        win.toggleWinnowFacetOption('category')

        win.toggleWinnowFacetOption('fastening')
        win.selectWinnowFacetOption('fastening', 'Velcro')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Fastening: Velcro')
        # misc.eyes_check_window(eyes, "UK ", 'Fastening: Velcro')
        win.toggleWinnowFacetOption('fastening')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        loadPage = 'sex/better-sex-for-her/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)


        # win.selectWinnowFacetOption('how-fast', 'Fast acting')  # Confirm category name and selector, this element isn't currently displayed
        # # misc.eyes_check_window(eyes, "UK ", 'How fast: Fast Acting')
        #
        # win.selectWinnowFacetOption('what-for', 'Stimulator')  # Confirm category name and selector, this element isn't currently displayed
        # # misc.eyes_check_window(eyes, "UK ", '+Whats it for: Stimulator')

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('pharmacy')
        win.selectWinnowFacetOption('pharmacy', 'Cream')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', '+Pharmacy drug type: Cream')
        # misc.eyes_check_window(eyes, "UK ", '+Pharmacy drug type: Cream')
        win.toggleWinnowFacetOption('pharmacy')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        # win.winnowClearAllClick() # Clear all will be needed when 'Fast acting' and/or 'what-for' apear as filter options
        # # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')


        loadPage = 'sex-toys/male-sex-toys/fleshlights/'
        win.get(URL + loadPage)
        print('Loading Page: ' + loadPage)
        misc.eyes_check_window_fluid(eyes, "UK ", loadPage)

        win.clickMobileWinnowFilter()
        win.toggleWinnowFacetOption('opening')
        win.selectWinnowFacetOption('opening', 'Butt')
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Opening: Butt')
        # misc.eyes_check_window(eyes, "UK ", 'Opening: Butt')
        win.toggleWinnowFacetOption('opening')

        win.clickMobileViewProducts()
        misc.eyes_check_window_fluid(eyes, "UK ", 'View Products clicked')

        win.clickMobileWinnowFilter()
        win.winnowClearAllClick()
        misc.eyes_check_region_by_element(wd, eyes, 'CSS', 'div#search-winnow div.sk-layout__filters', 'Clear All Filters')
        # misc.eyes_check_window(eyes, "UK ", 'Clear All Filters')

        print('Finished Test 4')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def copyAndAmendSpecialOffer(self, failures, offerName):
    wd = self.wd
    print('Copying & amending a Special Offer..')
    try:
        jibber.getJibber(wd)
        jibber.login(wd, 'qa.autotest', 'trying2FIX!')
        failures += jibber.jibberClickSpecialOffer(wd, failures)
        failures += jibber.copyOffer(wd, failures, offerName)
        print('Offer amended')
        print('==============')
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def createSpecialOffer(self, failures, offerName):
    wd = self.wd
    print('Creating a Special Offer...')
    try:
        jibber.getJibber(wd)
        jibber.login(wd, 'qa.autotest', 'trying2FIX!')
        failures += jibber.jibberClickSpecialOffer(wd, failures)
        failures += jibber.jibberAddAnOffer(wd, failures, offerName)
        print('Offer created')
        print('=============')
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


def expireSpecialOffer(self, failures, offerName):
    wd = self.wd
    print('Expiring a Special Offer..')
    try:
        jibber.getJibber(wd)
        jibber.login(wd, 'qa.autotest', 'trying2FIX!')
        failures += jibber.jibberClickSpecialOffer(wd, failures)
        failures += jibber.expireAnOffer(wd, failures, offerName)
        print('Offer expired')
        print('=============')
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def amendExpiredOffer(self, failures, offerName):
    wd = self.wd
    print('Ammending an expired Special Offer..')
    try:
        jibber.getJibber(wd)
        jibber.login(wd, 'qa.autotest', 'trying2FIX!')
        failures += jibber.jibberClickSpecialOffer(wd, failures)
        failures += jibber.changeExpiredOfferEndDate(wd, failures, offerName)
        print('Expired offer amended')
        print('=============')
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures


''' This function creates a Deal of the Day in the Merchandising App before checking it appears on the website'''
def check_deal_of_the_day(self, targetContainerID, productID, siteID, configureDotD):
    wd = self.wd
    failures = 0
    deal = DealPage(self.wd)

    try:
        if(configureDotD == True):
            print('Creating a DotD...')
            merch.getMerchandising(wd)
            merch.login(wd, 'qa.autotest', 'trying2FIX!')
            merch.selectDotD(wd)
            merch.addProduct(wd, productID, siteID)
            merch.logout(wd)
            time.sleep(10)  # Allow time for a newly created DotD to be added

        # print('Checking DotD is present...')
        if targetContainerID == 'beta':
            country = misc.getDotdURL(siteID)
        else:
            country = misc.getDotdURLContainer(siteID, targetContainerID)
        failures = misc.response_method(wd, country, failures)

        # merch.getMerchandising(wd)

    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def loginChecker(self, targetContainerID, eyes, site, username, password, scenario, failures):
    wd = self.wd
    wd.maximize_window()
    failures = 0

    print('Loggin in with: ' + scenario)
    print('Username: ' + username)
    print('Password: ' + password)

    try:
        # Opening eyes ready to check pages
        eyes.open(driver=wd, app_name='Applitools', test_name='Login: {}'.format(scenario))

        if targetContainerID == 'beta':
            accountslib.getAccountPage(wd, site)
        else:
            accountslib.getAccontPageContainer(wd, site, targetContainerID)
        accountslib.accountLogin(wd, username, password)
        misc.eyes_check_window_fluid(eyes, "{} ".format(site), scenario)
        time.sleep(1)
        print('Passed')
        print('======')

        try:
            eyes.close()
        except Exception as e:
            print(e.args)
            failures = failures + 1
    except:
        print('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

def checkpageresponsetimeCI(self, site, product, container, how_many_times_to_check_page, threshold):
    wd = self.wd
    wd.maximize_window()
    failures = 0
    try:
        failures = misc.checkpageloadtimeCI(self, site, product, container, how_many_times_to_check_page, threshold, failures)
    except:
        print ('fell over')
        print(traceback.print_exc())
        failures = failures + 1
    return failures

