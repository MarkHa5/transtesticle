# -*- coding: utf-8 -*-
import random, os
import string, requests
import testrail, checkoutlib, sql
from applitools.selenium import Eyes
from applitools.selenium import Target
from datetime import datetime
import uuid, traceback
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import selenium.webdriver.support.ui as UI
from page import *

#scrolls to the element based off the xpath passed in
def scroll_to_element_xpath(wd, elementXpath):
    element = wd.find_element_by_xpath(elementXpath)
    actions = ActionChains(wd)
    actions.move_to_element(element).perform()

#scrolls to the element based off the Id passed in
def scroll_to_element_id(wd, elementId):
    element = wd.find_element_by_id(elementId)
    actions = ActionChains(wd)
    actions.move_to_element(element).perform()

def scrollToElementLinkText(wd, elementLinkText):
    element = wd.find_element_by_link_text(elementLinkText)
    actions = ActionChains(wd)
    actions.move_to_element(element).perform()

def scrollToElementLinkText(wd, elementLinkText):
    element = wd.find_element_by_link_text(elementLinkText)
    actions = ActionChains(wd)
    actions.move_to_element(element).perform()

def click_out_side_element(wd, id):
    el = wd.find_element_by_id(id)
    action = ActionChains(wd)
    #action.move_to_element_with_offset(el, 5, 5)
    action.move_to_element_with_offset(el, 20, 20)
    action.click()
    action.perform()

def send_escape_key(wd):
    action = ActionChains(wd)
    action.send_keys(Keys.ESCAPE).perform()

#trys to close the blue country check box that appears when you open a site other than COUK
def check_country_screen(wd):
    try:
        wd.find_element_by_xpath('//*[@id="CountrySwitch"]/a').click()
    except:
        print ('no country screen present')
    time.sleep(1)

def name_the_screenshot_by_site(site):

    Names = {'CM': 'failscreenCOM',
             'CA': 'failscreenCA',
             'DE': 'failscreenDE',
             'ES': 'failscreenES',
             'NZ': 'failscreenNZ',
             'EU': 'failscreenEU',
             'FR': 'failscreenFR',
             'AU': 'failscreenAU',
             'UK': 'failscreenUK'
            }

    for site_name, screehshotname in Names.items():
        if site == site_name:
            print(screehshotname)
            return screehshotname

def nameScreenshotTest(site, test):
    name = 'failscreen_{site}_{test}'.format(site=site, test=test)
    return name

#returns the email for the site that the test is running on
def getEmail(site):
    email = ''
    if site == 'UK':
        email = 'auto.testercouk@lovehoney.co.uk'
    elif site == 'FR':
        email = 'auto.testerfr@lovehoney.co.uk'
    elif site == 'DE':
        email = 'auto.testerde@lovehoney.co.uk'
    elif site == 'CM':
        email = 'auto.testercom@lovehoney.co.uk'
    elif site == 'AU':
        email = 'auto.testercomau@lovehoney.co.uk'
    elif site == 'EU':
        email = 'auto.testereu@lovehoney.co.uk'
    elif site == 'CA':
        email = 'auto.testerca@lovehoney.co.uk'
    elif site == 'NZ':
        email = 'auto.testerconz@lovehoney.co.uk'
    elif site == 'ES':
        email = 'auto.testeres@lovehoney.co.uk'
    return email

def getEmailCheckout(site):
    email = ''
    if site == 'UK':
        email = 'auto.testercheckoutcouk@lovehoney.co.uk'
    elif site == 'FR':
        email = 'auto.testercheckoutfr@lovehoney.co.uk'
    elif site == 'DE':
        email = 'auto.testercheckoutde@lovehoney.co.uk'
    elif site == 'CM':
        email = 'auto.testercheckoutcom@lovehoney.co.uk'
    elif site == 'AU':
        email = 'auto.testercheckoutcomau@lovehoney.co.uk'
    elif site == 'EU':
        email = 'auto.testercheckouteu@lovehoney.co.uk'
    elif site == 'CA':
        email = 'auto.testercheckoutca@lovehoney.co.uk'
    elif site == 'NZ':
        email = 'auto.testercheckoutconz@lovehoney.co.uk'
    elif site == 'ES':
        email = 'auto.testercheckoutes@lovehoney.co.uk'
    return email

''' These email addresses are used by the Checkout Saved Card test '''
def getEmailCard(site):
    email = ''
    if site == 'UK':
        email = 'auto.testercardcouk@lovehoney.co.uk'
    elif site == 'FR':
        email = 'auto.testercardfr@lovehoney.co.uk'
    elif site == 'DE':
        email = 'auto.testercardde@lovehoney.co.uk'
    elif site == 'CM':
        email = 'auto.testercardcom@lovehoney.co.uk'
    elif site == 'AU':
        email = 'auto.testercardcomau@lovehoney.co.uk'
    elif site == 'EU':
        email = 'auto.testercardeu@lovehoney.co.uk'
    elif site == 'CA':
        email = 'auto.testercardca@lovehoney.co.uk'
    elif site == 'NZ':
        email = 'auto.testercardconz@lovehoney.co.uk'
    elif site == 'ES':
        email = 'auto.testercardes@lovehoney.co.uk'
    return email


#returns the email for the site that the test is running on
# def getEmailCi(site):
#     email = ''
#     if site == 'UK':
#         email = 'blah@blah.com'
#     elif site == 'FR':
#         email = 'ci@lovehoney.fr'
#     elif site == 'DE':
#         email = 'ci@lovehoney.de'
#     elif site == 'CM':
#         email = 'ci@lovehoney.com'
#     elif site == 'AU':
#         email = 'ci@lovehoney.com.au'
#     elif site == 'EU':
#         email = 'ci@lovehoney.eu'
#     elif site == 'CA':
#         email = 'ci@lovehoney.ca'
#     elif site == 'NZ':
#         email = 'ci@lovehoney.co.nz'
#     elif site == 'ES':
#         email = 'ci@lovehoney.es'
#     return email

#returns the email for the site that the test is running on
def getEmailLive(site):
    email = ''
    if site == 'UK':
        email = 'julian.bowles@lovehoney.co.uk'
    return email

def wait(site):
    if site =='UK':
        time.sleep(5)
    elif site == 'FR':
        time.sleep(10)
    elif site == 'DE':
        time.sleep(5)
    elif site == 'CM':
        time.sleep(5)
    elif site == 'AU':
        time.sleep(10)
    elif site == 'EU':
        time.sleep(0)
    elif site == 'CA':
        time.sleep(0)
    elif site == 'NZ':
        time.sleep(0)
    elif site == 'ES':
        time.sleep(10)

def gdpr_radio(wd):
    try:
        wd.find_element_by_id('marketingOptInNo').click()
    except:
        print ('No gdpr on this site, so didnt click the opt out')

#selects an option from a drop down box by passing in the Id of the drop down box and the option you want selected
def select_dropdown_option(wd, id, optiontext):
    selectdropdown = Select(wd.find_element_by_id(id))
    selectdropdown.select_by_visible_text(optiontext)

def select_dropdown_option_css(wd, css, optiontext):
    selectdropdown = Select(wd.find_element_by_css_selector(css))
    selectdropdown.select_by_visible_text(optiontext)

#select dropdown by xpath
def select_dropdown_option_xpath(wd, xpath, optiontext):
    selectdropdown = Select(wd.find_element_by_xpath(xpath))
    selectdropdown.select_by_visible_text(optiontext)

#uses the select_dropdown_option() method to select the correct country from the country selector dropdown box
def select_country_dropdown(wd, site):
    if site == 'UK':
        select_dropdown_option(wd, 'CountrySelect', 'United Kingdom')
    elif site == 'FR':
        select_dropdown_option(wd, 'CountrySelect', 'France')
    elif site == 'DE':
        select_dropdown_option(wd, 'CountrySelect', 'Deutschland')
    elif site == 'CM':
        select_dropdown_option(wd, 'CountrySelect', 'United States')
    elif site == 'AU':
        select_dropdown_option(wd, 'CountrySelect', 'Australia')
    elif site == 'EU':
        select_dropdown_option(wd, 'CountrySelect', 'Ireland')
    elif site == 'CA':
        select_dropdown_option(wd, 'CountrySelect', 'Canada')
    elif site == 'NZ':
        select_dropdown_option(wd, 'CountrySelect', 'New Zealand')
    elif site == 'ES':
        select_dropdown_option(wd, 'CountrySelect', u'España')

#opc select country function
def select_country_dropdown_opc(wd, site):
    if site == 'UK':
        select_dropdown_option(wd, 'deliverycountryId', 'United Kingdom')
    elif site == 'FR':
        select_dropdown_option(wd, 'deliverycountryId', 'France')
    elif site == 'DE':
        select_dropdown_option(wd, 'deliverycountryId', 'Deutschland')
    elif site == 'CM':
        select_dropdown_option(wd, 'deliverycountryId', 'United States')
    elif site == 'AU':
        select_dropdown_option(wd, 'deliverycountryId', 'Australia')
    elif site == 'EU':
        select_dropdown_option(wd, 'deliverycountryId', 'Ireland')
    elif site == 'CA':
        select_dropdown_option(wd, 'deliverycountryId', 'Canada')
    elif site == 'NZ':
        select_dropdown_option(wd, 'deliverycountryId', 'New Zealand')
    elif site == 'ES':
        select_dropdown_option(wd, 'deliverycountryId', u'España')


#executes a little script to select a checkbox, only used on the euro sites checkouts so far
def clickCheckbox(wd, id):
    checkbox = wd.find_element_by_id(id)
    wd.execute_script("arguments[0].click()", checkbox)

#executes a little script to select a checkbox, only used on the euro sites checkouts so far
def clickCheckboxXpath(wd, xpath):
    checkbox = wd.find_element_by_xpath(xpath)
    wd.execute_script("arguments[0].click()", checkbox)

#executes a little script to select a checkbox, only used on the euro sites checkouts so far
def clickCheckboxCss(wd, css):
    checkbox = wd.find_element_by_css_selector(css)
    wd.execute_script("arguments[0].click()", checkbox)

#passes in the before and after loyalty points and checks that the correct amount has been deducted
def checkPointsDeduction(before, after, pointsUsed, failures):
    if int(after) == int(before) - int(pointsUsed):
        print ('Points deducted correctly: PASSED')
    else:
        print ('Points not deducted correctly: FAILED. Before:{} Used:{} After:{}'.format(before, pointsUsed, after))
        failures = failures + 1
    return failures

#opens the homepage for the site passed in
def getHome(wd, site):
    if site == 'UK':
        wd.get('http://lovehoneycouk.beta')
    elif site == 'FR':
        wd.get('http://lovehoneyfr.beta')
    elif site == 'DE':
        wd.get('http://lovehoneyde.beta')
    elif site == 'CM':
        wd.get('http://lovehoneycom.beta')
    elif site == 'AU':
        wd.get('http://lovehoneycomau.beta')
    elif site == 'EU':
        wd.get('http://lovehoneyeu.beta')
    elif site == 'CA':
        wd.get('http://lovehoneyca.beta')
    elif site == 'NZ':
        wd.get('http://lovehoneyconz.beta')
    elif site == 'ES':
        wd.get('http://lovehoneyes.beta')

def getDotdURL(site):
    if site == 'UK':
        return 'http://lovehoneycouk.beta/deal-of-the-day/'
    elif site == 'FR':
        return 'http://lovehoneyfr.beta/promo-du-jour/'
    elif site == 'DE':
        return 'http://lovehoneyde.beta/deal-des-tages/'
    elif site == 'CM':
        return 'http://lovehoneycom.beta/deal-of-the-day/'
    elif site == 'AU':
        return 'http://lovehoneycomau.beta/deal-of-the-day/'
    elif site == 'EU':
        return 'http://lovehoneyeu.beta/deal-of-the-day/'
    elif site == 'CA':
        return 'http://lovehoneyca.beta/deal-of-the-day/'
    elif site == 'NZ':
        return 'http://lovehoneyconz.beta/deal-of-the-day/'
    elif site == 'ES':
        return 'http://lovehoneyes.beta/oferta-del-dia/'

def getDotdURLContainer(site, containerId):
    if site == 'UK':
        return 'https://lovehoneycouk-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'FR':
        return 'https://lovehoneyfr-{}.container.ci/promo-du-jour/'.format(containerId)
    elif site == 'DE':
        return 'https://lovehoneyde-{}.container.ci/deal-des-tages/'.format(containerId)
    elif site == 'CM':
        return 'https://lovehoneycom-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'AU':
        return 'https://lovehoneycomau-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'EU':
        return 'https://lovehoneyeu-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'CA':
        return 'https://lovehoneyca-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'NZ':
        return 'https://lovehoneyconz-{}.container.ci/deal-of-the-day/'.format(containerId)
    elif site == 'ES':
        return 'https://lovehoneyes-{}.container.ci/oferta-del-dia/'.format(containerId)

# opens the homepage for the site passed in
# def getHomeCi(wd, site):
#     if site == 'UK':
#         wd.get('http://lovehoneycouk.ci')
#     elif site == 'FR':
#         wd.get('http://lovehoneyfr.ci')
#     elif site == 'DE':
#         wd.get('http://lovehoneyde.ci')
#     elif site == 'CM':
#         wd.get('http://lovehoneycom.ci')
#     elif site == 'AU':
#         wd.get('http://lovehoneycomau.ci')
#     elif site == 'EU':
#         wd.get('http://lovehoneyeu.ci')
#     elif site == 'CA':
#         wd.get('http://lovehoneyca.ci')
#     elif site == 'NZ':
#         wd.get('http://lovehoneyconz.ci')
#     elif site == 'ES':
#         wd.get('http://lovehoneyes.ci')

#opens the help page for the site passed in
def getHelpPage(wd, site):
    if site == 'UK':
        wd.get('http://lovehoneycouk.beta/help')
    elif site == 'FR':
        wd.get('http://lovehoneyfr.beta/aide')
    elif site == 'DE':
        wd.get('http://lovehoneyde.beta/hilfe')
    elif site == 'CM':
        wd.get('http://lovehoneycom.beta/help')
    elif site == 'AU':
        wd.get('http://lovehoneycomau.beta/help')
    elif site == 'EU':
        wd.get('http://lovehoneyeu.beta/help')
    elif site == 'CA':
        wd.get('http://lovehoneyca.beta/help')
    elif site == 'NZ':
        wd.get('http://lovehoneyconz.beta/help')
    elif site == 'ES':
        wd.get('http://lovehoneyes.beta/ayuda')

def getHelpPageContainer(wd, site, targetContainerID):
    if site == 'UK':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'FR':
        wd.get('https://lovehoneycouk-{}.container.ci/aide'.format(targetContainerID))
    elif site == 'DE':
        wd.get('https://lovehoneycouk-{}.container.ci/hilfe'.format(targetContainerID))
    elif site == 'CM':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'AU':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'EU':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'CA':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'NZ':
        wd.get('https://lovehoneycouk-{}.container.ci/help'.format(targetContainerID))
    elif site == 'ES':
        wd.get('https://lovehoneycouk-{}.container.ci/ayuda'.format(targetContainerID))

# opens the help page for the site passed in
# def getHelpPageCi(wd, site):
#     if site == 'UK':
#         wd.get('http://lovehoneycouk.ci/help')
#     elif site == 'FR':
#         wd.get('http://lovehoneyfr.ci/aide')
#     elif site == 'DE':
#         wd.get('http://lovehoneyde.ci/hilfe')
#     elif site == 'CM':
#         wd.get('http://lovehoneycom.ci/help')
#     elif site == 'AU':
#         wd.get('http://lovehoneycomau.ci/help')
#     elif site == 'EU':
#         wd.get('http://lovehoneyeu.ci/help')
#     elif site == 'CA':
#         wd.get('http://lovehoneyca.ci/help')
#     elif site == 'NZ':
#         wd.get('http://lovehoneyconz.ci/help')
#     elif site == 'ES':
#         wd.get('http://lovehoneyes.ci/ayuda')

#opens the forum home page on LHUK
def getForumHome(wd):
    wd.get('http://lovehoneycouk.beta/community/forums/')

def getSiteId(wd, site):
    if site == 'UK':
        return 1
    elif site == 'FR':
        return 16
    elif site == 'DE':
        return 28
    elif site == 'CM':
        return 62
    elif site == 'AU':
        return 71
    elif site == 'EU':
        return 74
    elif site == 'CA':
        return 5
    elif site == 'NZ':
        return 75
    elif site == 'ES':
        return 76



#this will attempt to log the user out if they are logged in
def logOut(wd, site, login):
    time.sleep(5)
    try:
        if login == 'LIN':
            if site == 'FR':
                wd.find_element_by_link_text(u'Se déconnecter').click()
            elif site == 'DE':
                wd.find_element_by_link_text('Abmelden').click()
            elif site == 'ES':
                wd.find_element_by_link_text(u'Cerrar Sesión').click()
            else:
                wd.find_element_by_link_text('Log Out').click()
            time.sleep(4)
    except:
        print("Couldn't click logout button")


#same as logOut() but for mobile
def logOutMobile(wd, site, login):
    try:
        if login == 'LIN':
            getHome(wd, site)
            scroll_to_element_xpath(wd, '//*[@id="header"]/div[1]/div[1]/button[1]/span')
            wd.find_element_by_xpath('//*[@id="header"]/div[1]/div[1]/button[1]/span').click()
            time.sleep(5)
            if site == 'FR':
                scrollToElementLinkText(wd, u'Se déconnecter')
                wd.find_element_by_link_text(u'Se déconnecter').click()
            elif site == 'DE':
                scrollToElementLinkText(wd, 'Abmelden')
                wd.find_element_by_link_text('Abmelden').click()
            elif site == 'ES':
                scrollToElementLinkText(wd, u'Cerrar Sesión')
                wd.find_element_by_link_text(u'Cerrar Sesión').click()
            else:
                scrollToElementLinkText(wd, 'Log Out')
                wd.find_element_by_link_text('Log Out').click()
            time.sleep(10)
    except:
        print("Couldn't click logout button")


#generates a random string of the length passed in
def randomword(length):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))

#uses randomword() to create a random lovehoney email address
def random_email():
    randomString = randomword(6)
    randomEmail = randomString + '@lovehoney.co.uk'
    print (randomEmail)
    return randomEmail

#checks that when an account is opened it is awarded the correct amount of points
def checkOpenAccountPoints(accountid, failures):
    points = sql.getOpenAccountPoints(accountid)
    if points == 2500:
        print ('PASSED: Correct amount of points awarded')
    else:
        print ('FAILED: Wrong amount of points awarded')
        failures = failures + 1
    return failures

#uses testrail api, to update the results to testrail based off the case Id, and testrun Id
def updateTestRail(caseID, testRun, result, log):
    # TestRail URL:
    client = testrail.APIClient('https://lovehoney2.testrail.net/')
    #client = testrail.APIClient('https://lovehoney2.testrail.net/')
    # TestRail user to connect:
    client.user = 'julian.bowles@lovehoney.co.uk'
    # TestRail API Key:
    client.password = 'eGMqlIYPfGunG2.DDizf-gIzj18utmnJLBTltgfam'
    # Getting the test case
    case = client.send_get('get_case/{0}'.format(caseID))
    # Printing some details about test case
    print(case)
    # Updating test case and test run and putting in a comment
    result = client.send_post(
        'add_result_for_case/{0}/{1}'.format(testRun,caseID),
        {'status_id': result, 'comment': log}
    )
    # Printing some stuff about have the test case and test run were updated
    print(result)

def createTestRun():

    testRun = os.environ.get('TEST_RUN_ID', 'TEST - Beta Overnights')

    print('Creating test run, name = ' + testRun)
    # TestRail URL:
    client = testrail.APIClient('https://lovehoney2.testrail.net/')
    # client = testrail.APIClient('https://lovehoney2.testrail.net/')
    # TestRail user to connect:
    client.user = 'julian.bowles@lovehoney.co.uk'
    # TestRail API Key:
    client.password = 'eGMqlIYPfGunG2.DDizf-gIzj18utmnJLBTltgfam'

    result = client.send_post('add_run/1', {'name': testRun, 'include_all': False, 'case_ids': [408193, 1813]})

    print(result)


#uses failures to determine whether to send the pass or fail result to testrail using updateTestRail()
def sendResultsTR(failures, result_string, case, run):
    if failures != 0:
        updateTestRail(case, run, "5", result_string)
    else:
        updateTestRail(case, run, "1", result_string)

#gets all the elements of the passed in tag within the passed in xpath and stores them in an array
#the array is then iterated through and the response_method() is used to check the responses of the pages
def checkPageLinks(wd, xpath, tag, failures):
    body = wd.find_element_by_xpath(xpath)
    all_links = body.find_elements_by_tag_name(tag)
    arrayMain = []
    for i in all_links:
        arrayMain.append(i.get_attribute('href'))
    for i in arrayMain:
        wd.get(i)
        # print ('-'*5)
        # print (i)
        failures = response_method(wd, i, failures)
    # print ('-'*10)
    return failures

#uses the page passed in as 'i' and checks the response given back when the page is requested
def response_method(wd, i, failures):
    wd.get(i)
    # For when this method is used for testing images, its its a lazy load image or a non product image dont test
    if "data:image" in i or "bing.com" in i or "secimg." in i or "loh." in i:
        print("Not testing this as its a lazy load image or a non product image")
    else:
        response = requests.get(i, verify=False)
        # print (response.status_code)
        # print (i)
        if wd.current_url == i:
            pass
            # print ('PASS - Expected URL')
        else:
            print(i)
            if '301' in str(response.history):
                print ('PASS - 301 redirected link')
            else:
                print ('FAIL - Not a 301 redirect')
                failures = failures+1
        if str(response.status_code) == '404':
            if wd.current_url == 'https://lovehoneycomau.beta/help/loyalty-program-faq/':
                print('Ignoring https://lovehoneyeu.beta/help/loyalty-program-faq/')
            elif wd.current_url == 'https://lovehoneyeu.beta/help/loyalty-program-faq/':
                print('Ignoring https://lovehoneyeu.beta/help/loyalty-program-faq/')
            else:
                failures = failures + 1
                print(i)
                print("FAIL - 404 Page not found")
        elif str(response.status_code) == '500':
            print(i)
            failures = failures + 1
            print("FAIL - 500 Potential lucee error please investigate")
        elif str(response.status_code) == '200':
            pass
            # print("PASS - 200 OK")
        elif str(response.status_code) == '502':
            print(i)
            failures = failures + 1
            print("FAIL - 502 Bad Gateway")
        elif str(response.status_code) == '403':
            print(i)
            print ("Access denied")
            if "userimages" in i:
                print ("This is a user image from the community page so access denied is expected as we are not logged in")
            else:
                failures = failures + 1
                print ("FAIL Was not expecting a Access denied for this link")
        else:
            print(i)
            failures = failures + 1
            print("Got a response that was not expected please investigate - FAIL")
    return failures

#opens the home page for the brochureware site passed in
def getBrochurewareHomePage(wd, site):
    if site == 'LHG':
        wd.get('http://lovehoneygroupcom.beta/')
    elif site == 'SWN':
        wd.get('http://swoontv.beta/')
    elif site == 'SQW':
        wd.get('http://sqweelcom.beta/')

#opens the contact us page for the brochureware site passed in
def getBrochurewareContactPage(wd, site):
    if site == 'LHG':
        wd.get('http://lovehoneygroupcom.beta/help/contact-us/')
    elif site == 'SWN':
        wd.get('http://swoontv.beta/contact-us/')
    elif site == 'SQW':
        wd.get('http://sqweelcom.beta/contact/')

#Get current time
def getTime():
    date_time = datetime.now().strftime('%Y-%m-%d %H:')
    return date_time

#Get any page
def getPage(wd, page):
    wd.get(page)

#Get all specific page elements that are http links
def getPageElementsHttpLinks(wd, xpath, tag):
    if xpath != 'no':
        element_loc = wd.find_element_by_xpath(xpath)
        all_links = element_loc.find_elements_by_tag_name(tag)
    else:
        all_links = wd.find_elements_by_tag_name('img')
    return all_links

#Make an array of href links and check them
def checkAllGivenHrefs(wd, all_links, attribute, failures):
    # Below is creating an array called 'a' to hold the links we have found
    a = []
    for i in all_links:
        boom = i.get_attribute(attribute)
        # Below is putting each of the links into the array using the append method
        a.append(boom)
    #Going thru the links in the array and checking they go where we think they should
    for i in a:
        print ("Testing this link {0}".format(i))
        failures = response_method(wd, i, failures)
    return failures

def checkForExpected404(wd, page, failures):
    print ("Page being tested that should give a 404 is {0}".format(page))
    wd.get(page)
    response = requests.get(page, verify=False)
    print (response.status_code)
    if wd.current_url == page:
        print ('PASS - Expected URL')
    else:
        if '301' in str(response.history):
            print ('PASS - 301 redirected link')
        else:
            print ('FAIL - Not a 301 redirect')
            failures = failures + 1
    if str(response.status_code) == '404':
        print("PASS - We expect this page to 404")
    elif str(response.status_code) == '500':
        failures = failures + 1
        print("500 Potential lucee error please investigate - FAIL")
    elif str(response.status_code) == '200':
        failures = failures + 1
        print("FAIL - we were expecting the page to not be a 200 OK, it should have been a 404")
    else:
        failures = failures + 1
        print("Got a response that was not expected please investigate - FAIL")
    return failures

#Make selenium wait for an element to be present on page and will click it once availble if required (will wait for max 10 seconds)
def waitItOut(wd, element, element_type, click, failures):
    pass_message = "PASS - the element you are waiting for did show up"
    try:
        if element_type == "XPATH":
            WebDriverWait(wd, 10).until(EC.presence_of_element_located((By.XPATH, element)))
            print ("Successfully waited for {0}".format(element))
            if click == "YES":
                wd.find_element_by_xpath(element).click()
                print (pass_message)
        elif element_type == "ID":
            WebDriverWait(wd, 10).until(EC.presence_of_element_located((By.ID, element)))
            print ("Successfully waited for {0}".format(element))
            if click == "YES":
                wd.find_element_by_id(element).click()
                print (pass_message)
        elif element_type == "CSS":
            WebDriverWait(wd, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, element)))
            print ("Successfully waited for {0}".format(element))
            if click == "YES":
                wd.find_element_by_css_selector(element).click()
                print (pass_message)
        else:
            element = WebDriverWait(wd, 10).until(EC.presence_of_element_located((By.LINK_TEXT, element)))
            print ("Successfully waited for {0}".format(element))
            if click == "YES":
                wd.find_element_by_css_selector(element).click()
                print (pass_message)
    except:
        failures += 1
        print("FAIL - the element you are waiting for never showed up")
    return failures


#To reinapp and reload all sites prior to running the test
def reintandreloadsites(wd, failures):

    sites = ["https://lovehoneycouk.beta/?reinitapp=true&reload=true",
             "https://lovehoneycouk.beta/product.cfm?p=319",
             "https://lovehoneyfr.beta/?reinitapp=true&reload=true",
             "https://lovehoneyfr.beta/product.cfm?p=319",
             "https://lovehoneyde.beta/?reinitapp=true&reload=true",
             "https://lovehoneyde.beta/product.cfm?p=319",
             "https://lovehoneycom.beta/?reinitapp=true&reload=true",
             "https://lovehoneycom.beta/product.cfm?p=319",
             "https://lovehoneycomau.beta/?reinitapp=true&reload=true",
             "https://lovehoneycomau.beta/product.cfm?p=319",
             "https://lovehoneyeu.beta/?reinitapp=true&reload=true",
             "https://lovehoneyeu.beta/product.cfm?p=319",
             "https://lovehoneyca.beta/?reinitapp=true&reload=true",
             "https://lovehoneyca.beta/product.cfm?p=319",
             "https://lovehoneyconz.beta/?reinitapp=true&reload=true",
             "https://lovehoneyconz.beta/product.cfm?p=319",
             "https://lovehoneyes.beta/?reinitapp=true&reload=true",
             "https://lovehoneyes.beta/product.cfm?p=319"
             ]
    try:
        for site in sites:
            wd.get(site)
            response_method(wd, site, failures)
    except:
        failures += 1
        print ("One of the sites failed to reload cleanly")
    return failures

def fraudMail_to_checkout(wd, fraud, failures):
    try:
        if fraud == "yes":
            checkoutlib.loginCheckoutHD(wd, "automatedfraud@lovehoney.co.uk", "LOT")
        else:
            email = random_email()
            checkoutlib.loginCheckoutHD(wd, email, "LOT")
    except:
        failures += 1
        print ("One of the sites failed to reload cleanly")
    return failures

#Returns a random string of length string_length
def my_random_string(string_length=10):
    random = str(uuid.uuid4())  # Convert UUID format to a Python string.
    random = random.upper()  # Make all characters uppercase.
    random = random.replace("-", "")  # Remove the UUID '-'.
    return random[0:string_length]  # Return the random string.

def check_if_order_suspended(site, order_status, overide_suspend_due_to_reasons, failures):
    if overide_suspend_due_to_reasons == "yes":
        print("Checking the orders that should not be suspended")
        if order_status == 2:
            print("PASS - Order has not been suspended")
        else:
            failures = failures + 1
            print("FAIL - Order has been suspended")
    # Test for orders that should be suspended
    else:
        print("Checking that orders that should be suspended with the exception of AUS")
        if "AU" not in site:
            if order_status == 1:
                print("PASS - Order has been suspended")
            else:
                failures = failures + 1
                print("FAIL - Order was not suspended")
        else:
            if order_status == 2:
                print("PASS - Its an AUS Order and wont be suspended and it has not been")
            else:
                failures = failures + 1
                print("FAIL - Its an AUS Order it should have been unsuspended")
    return failures

def check_order_suspend_reason(site, fraud, suspend_reason, overide_sus_3ds, fr_suspend_rule, failures):
    if overide_sus_3ds == "no":
        if site == "FR":
            if fr_suspend_rule == "sec":
                if suspend_reason == 2:
                    print("PASS - Order has been given the suspend reason of Security required")
                else:
                    failures = failures + 1
                    print("FAIL - French order has not been given the suspend reason of Security required")


            elif fraud == "yes":
                if suspend_reason == 39:
                    print ("PASS - This French Blacklisted customer email address order has been the correct suspend reason")
                else:
                    failures = failures +1
                    print ("FAIL - This French order has not been given the suspend reason we expect")
            elif suspend_reason == 1:
                print ("PASS - This French order has been given the suspend reason of Foreign which is correct")
            else:
                failures = failures +1
                print ("FAIL - This French order has not been given the suspend reason we expect")



        elif fraud == "yes":
            if suspend_reason == 39:
                print ("PASS - Blacklisted customer email address order has been given the correct suspend reason")
            else:
                failures = failures + 1
                print (suspend_reason)
                print ("FAIL - Blacklisted customer email address order has not been given the suspend reason we expect")
        elif suspend_reason == 2:
            print("PASS - Order has been given the suspend reason of Security required")
        else:
            failures = failures + 1
            print("FAIL - Order has not been given the suspend reason of Security required")
    return failures


def check_order_payment_text(suspendlineone, cv2_message, failures):
    if suspendlineone == "201":
        if cv2_message == "DATA NOT CHECKED":
            print(
                "PASS - We were testing the Braintree data not checked reason and the payment text is showing : Success (DATA NOT CHECKED)")
        else:
            failures = failures + 1
            print(
                "FAIL - We were testing the Braintree data not checked reason and the payment text is not showing : Success (DATA NOT CHECKED)")

    elif suspendlineone == "200":
        if cv2_message == "NO DATA MATCHES":
            print(
                "PASS - We were testing the Braintree Does not match reason and the payment text is showing : Success (NO DATA MATCHES)")
        else:
            failures = failures + 1
            print(
                "FAIL - We were testing the Braintree Does not match reason and the payment text is not showing : Success (NO DATA MATCHES)")
    return failures

def checkTaxValue(expectedVal, checkoutVal, grandTotal, failures):

    if checkoutVal != expectedVal:
        print('US Sales Tax mismatch! GrandTotal: {} Expected value: {}  checkout value: {} '.format(grandTotal, expectedVal, checkoutVal))
        print('-----------------------------------------------------------------------')
        failures = failures + 1
    else:
        print('US Sales Tax is correct')
    return failures

def my_random_pc():
    RandomPC = my_random_string(6)
    firstletter = "A"
    newpostcode = ("{}{}".format(firstletter, RandomPC))
    return newpostcode

def which_post_checkout_button(wd, site, failures):
    # Xpaths to get to the post checkout bribe button
    LHUK_BETA = '//*[@id="blob527"]/ol/li[2]/form/fieldset/div/button'
    LHCOM_LHAUS_LHEU_BETA = '//*[@id="main"]/div[1]/ol/li[1]/form/fieldset/div/button'
    LHDE_LHFR_BETA = '//*[@id="blob527"]/ol/li[1]/form/fieldset/div/button'
    LHNZ_BETA = '//*[@id="main"]/div[1]/ol/li[1]/form/fieldset/div/button'
    try:
        if "UK" in site:
            wd.find_element_by_xpath(LHUK_BETA).click()
        elif "CM" in site:
            wd.find_element_by_xpath(LHCOM_LHAUS_LHEU_BETA).click()
        elif "AU" in site:
            wd.find_element_by_xpath(LHCOM_LHAUS_LHEU_BETA).click()
        elif "EU" in site:
            wd.find_element_by_xpath(LHCOM_LHAUS_LHEU_BETA).click()
        elif "DE" in site:
            wd.find_element_by_xpath(LHDE_LHFR_BETA).click()
        elif "FR" in site:
            wd.find_element_by_xpath(LHDE_LHFR_BETA).click()
        elif "CA" in site:
            wd.find_element_by_xpath(LHCOM_LHAUS_LHEU_BETA).click()
        elif "NZ" in site:
            wd.find_element_by_xpath(LHNZ_BETA).click()
        elif "ES" in site:
            wd.find_element_by_xpath(LHDE_LHFR_BETA).click()
    except:
        failures += 1
        print ("Couldn't find the post {} checkout button".format(site))
    return failures

def check_for_railo_or_404(wd, failures):
    strings = ("Railo", "Lucee", "404 Page")
    if any(s in wd.find_element_by_tag_name("html").text for s in strings):
        failures = failures + 1
        print("FAIL Lucee Error or 404")
    else:
        print("PASS - No Lucee Error or 404")
    return failures


def find_string_on_page(wd, stringtofind, failures):
    if not (stringtofind in wd.find_element_by_tag_name("html").text):
        failures = failures + 1
        print("FAIL - Not the correct string found which was = {}".format(stringtofind))
    else:
        print("PASS - Correct string was found =  {}".format(stringtofind))
    return failures

def get_community_page(wd):
    wd.get("https://lovehoneycouk.beta/community")

def get_community_pageContainer(wd, targetContainerID):
    wd.get("https://lovehoneycouk-{}.container.ci/community".format(targetContainerID))

def get_number_of_products_from_PLP(wd):
    body = wd.find_element_by_xpath('//*[@id="main"]')
    all_links = body.find_elements_by_tag_name('a')
    arrayMain = []
    for i in all_links:
        if "product" in i.get_attribute('href'):
            arrayMain.append(i.get_attribute('href'))
    No_products_displayed = (len(set(arrayMain)))
    return No_products_displayed

def get_number_of_products_from_PLP_react(wd):
    time.sleep(3)
    element = wd.find_element_by_css_selector(".hits-stats")
    full_string = element.text
    #Spliting the string from the element
    do = full_string.split()
    #Giving each part of the text split an index
    a,b,c,d,e,f,g = do
    #Using the f index to get the number of products displayed from the string
    No_products_displayed = f
    return No_products_displayed

#Checks a whole page using applitools eyes
def eyes_check_window(eyes, site, pagename):
    time.sleep(4)
    eyes.check_window(site+pagename)

# Checks a whole page using applitools eyes
def eyes_check_window_fluid(eyes, site, pagename):
    time.sleep(4)
    eyes.check(site+pagename, Target.window().ignore_displacements(True).ignore_caret(True))

#Checks a specific element on a page using applitools eyes
def eyes_check_region_by_element(wd, eyes, elementtype, element, tag):
    if elementtype == "CSS":
        time.sleep(1)
        checkthiscss = wd.find_element_by_css_selector(element)
        # eyes.check_region_by_element(checkthiscss, tag)
        # the function below it's a substitute for the one above because of the upgrade of eyes
        eyes.check_region(checkthiscss, tag)
    elif elementtype == "XPATH":
        time.sleep(1)
        checkthisxpath = wd.find_element_by_xpath(element)
        # eyes.check_region_by_element(checkthisxpath, tag)
        # the function below it's a substitute for the one above because of the upgrade of eyes
        eyes.check_region(checkthisxpath, tag)
    elif elementtype == "ID":
        time.sleep(1)
        checkthisid = wd.find_element_by_xpath(element)
        # eyes.check_region_by_element(checkthisid, tag)
        # the function below it's a substitute for the one above because of the upgrade of eyes
        eyes.check_region(checkthisid, tag)

def turnRecaptchaOff(wd, site):
    if site == 'UK':
        wd.get('https://lovehoneycouk.staging/listen/disable_recaptcha.cfm?set=true')
    elif site == 'CM':
        wd.get('https://lovehoneycom.staging/listen/disable_recaptcha.cfm?set=true')
    time.sleep(3)

def turnRecaptchaOn(wd, site):
    if site == 'UK':
        wd.get('https://lovehoneycouk.staging/listen/disable_recaptcha.cfm?set=false')
    elif site == 'CM':
        wd.get('https://lovehoneycom.staging/listen/disable_recaptcha.cfm?set=false')
    time.sleep(3)

def turnRecaptchaOffBeta(wd, site):
    if site == 'UK':
        wd.get('https://lovehoneycouk.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'FR':
        wd.get('https://lovehoneyfr.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'DE':
        wd.get('https://lovehoneyde.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'CM':
        wd.get('https://lovehoneycom.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'AU':
        wd.get('https://lovehoneycomau.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'EU':
        wd.get('https://lovehoneyeu.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'CA':
        wd.get('https://lovehoneyca.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'NZ':
        wd.get('https://lovehoneyconz.beta/listen/disable_recaptcha.cfm?set=true')
    elif site == 'ES':
        wd.get('https://lovehoneyes.beta/listen/disable_recaptcha.cfm?set=true')
    time.sleep(3)

def turnRecaptchaOnBeta(wd, site):
    if site == 'UK':
        wd.get('https://lovehoneycouk.beta/listen/disable_recaptcha.cfm?set=false')
    elif site == 'CM':
        wd.get('https://lovehoneycom.beta/listen/disable_recaptcha.cfm?set=false')
    time.sleep(3)

def get_clear_browsing_button(wd):
    """Find the "CLEAR BROWSING BUTTON" on the Chrome settings page."""
    return wd.find_element_by_css_selector('* /deep/ #clearBrowsingDataConfirm')

#Required to navigate the shadow dom
def expand_shadow_element(wd, element):
  shadow_root = wd.execute_script('return arguments[0].shadowRoot', element)
  return shadow_root

def clear_cache(wd, timeout=60):
    """Clear the cookies and cache for the ChromeDriver instance."""
    wd.get('chrome://settings/clearBrowserData')
    #Waiting for dialogue to load
    time.sleep(2)

    #Navigating down the shadow dom
    root1 = wd.find_element_by_css_selector('settings-ui')
    shadow_root1 = expand_shadow_element(wd, root1)

    root2 = shadow_root1.find_element_by_css_selector('settings-main')
    shadow_root2 = expand_shadow_element(wd, root2)

    root3 = shadow_root2.find_element_by_css_selector('settings-basic-page')
    shadow_root3 = expand_shadow_element(wd, root3)

    root4 = shadow_root3.find_element_by_css_selector("settings-section[page-title='Privacy and security']")

    root5 = root4.find_element_by_css_selector('settings-privacy-page')
    shadow_root5 = expand_shadow_element(wd, root5)

    root6 = shadow_root5.find_element_by_css_selector('settings-clear-browsing-data-dialog')
    shadow_root6 = expand_shadow_element(wd, root6)

    root7 = shadow_root6.find_element_by_css_selector('cr-dialog')
    search_button = root7.find_element_by_css_selector('#clearBrowsingDataConfirm')
    search_button.click()

    #Waiting for the clear cache to finish
    time.sleep(15)

def checkpageloadtime(wd, url, how_many_times_to_check_page, threshold, failures):
    loadtime_threshold = threshold
    #Making threshold figure into seconds
    threshold_in_seconds = (threshold / 1000.0)
    arraysList = []
    for homepage in range(0, how_many_times_to_check_page, +1):
        wd.get(url)
        loadtime = wd.execute_script("return performance.timing.loadEventEnd - performance.timing.navigationStart;")
        arraysList.append(loadtime)
        # Clearing the cache ready for next test
        clear_cache(wd)
    average = sum(arraysList) / len(arraysList)
    #Making the average figure into seconds
    average_in_seconds = (average / 1000.0)
    print ('The average load time after {0} trys is {1} seconds'.format(how_many_times_to_check_page, average_in_seconds))
    if average > loadtime_threshold:
        print ("Average response time of {0} seconds  was > the set threshold which is {1} so this is a fail".format(average, threshold_in_seconds))
        failures = failures+1
    else:
        print(
        "Page load average time after {2} tries was {0} this is an acceptable response time because it was less than the threshold set to {1} seconds".format(average_in_seconds, threshold_in_seconds, how_many_times_to_check_page))
    return failures

def wait_for_and_click_alert(wd, text):
    alert = WebDriverWait(wd, 3).until(EC.alert_is_present(), text)
    alert.accept()

def check_and_count_links_by_tag_name(wd, wbsite, tag, expected_links, failures):
    wd.get(wbsite)
    # Getting all the links in the footer and counting them
    footer = wd.find_element_by_xpath('//*[@id="footer"]')
    all_links = footer.find_elements_by_tag_name(tag)
    total_links = len(all_links)

    if not total_links == expected_links:
        failures += 1
        print("Different number of links on page should be %s - FAIL" % expected_links)
        print("Actual number of links counted on page %s" % total_links)
    else:
        print("Same number of links as we expect - PASS")
    # Below is creating an array called 'a'
    a = []
    for i in all_links:
        print (i.get_attribute('href'))
        boom = i.get_attribute('href')
        # Below is putting each of the links into the array using the append method
        a.append(boom)
    for i in a:
        if i is None:
            print ("Nothing to see here, got a none link so moving on...")
        else:
            response_method(wd, i, failures)
    return failures

#To reinapp and reload all sites prior to running the test
def reintandreloadsites_containers(wd, branch):
    failures = 0
    sites = ["https://lovehoneycouk-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyfr-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyde-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneycom-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneycomau-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyeu-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyca-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyconz-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             "https://lovehoneyes-{0}.container.ci/?reinitapp=true&reload=true".format(branch),
             ]
    try:
        for site in sites:
            wd.get(site)
            response_method(wd, site, failures)
    except:
        print ("One of the sites failed to reload cleanly")
        failures = 1
    return failures


def accept_cookies(wd, site, failures):
    time.sleep(2)
    try:
        wd.find_element_by_css_selector("body > div.cookie-banner > div > button").click()
        print("Clicked the accept cookies button")
    except:
        print("Accept cookies button is not found")
    return failures

def checkCountrySaved(wd, failures):
    try:
        # BELOW USED FOR GRABBING DROPDOWN CONTENT
        select_box = UI.Select(wd.find_element_by_id('CountrySelect'))
        countries = []
        for option in select_box.options:
            countries.append(option.get_attribute('text'))
        wd.back()
        time.sleep(2)

        for country in countries:
            print(country)
            # Clicking on the saved address edit button
            wd.find_element_by_xpath("//*[@id='addressList']/div/fieldset/address/span/ol[1]/li/a").click()
            time.sleep(2)
            select_dropdown_option(wd, "CountrySelect", country)
            time.sleep(2)
            # USA requires a state to be choosen from a drop down
            if country == "United States":
                # utils.select_dropdown_option(wd, "UsState", "ID - Idaho")
                select_dropdown_option(wd, "UsState", "ID - Idaho")

            if country == "Australia":
                # utils.select_dropdown_option(wd, "AuState", "NSW - New South Wales")
                select_dropdown_option(wd, "AuState", "NSW - New South Wales")
            # Clicking the save button
            time.sleep(2)
            wd.find_element_by_xpath("//*[@id='main']/form/fieldset/ol[6]/li/button").click()
            # Waiting for page to retrun to view saved addresses after saving
            wd.find_element_by_xpath("//div[@id='content']//h2[.='Manage your address book']").click()
            # Checking that the country slected in the drop down on the edit address page is saved correctly
            if 'United Kingdom' in country:
                print('UK so skip check step as not visible')
            else:
                if not (country in wd.find_element_by_tag_name("html").text):
                    failures = failures + 1
                    print("Wrong country has been saved - FAIL")
                else:
                    print("Looks like the countries match! - PASS")
    except:
        failures += 1
        print("Looks like something went wrong when trying to find text in a URL")
    return failures

def checkpageloadtimeCI(self, site, product, container, how_many_times_to_check_page, threshold, failures):
    productPage = ProductPage(self.wd)
    loadtime_threshold = threshold
    #Making threshold figure into seconds
    threshold_in_seconds = (threshold / 1000.0)
    arraysList = []
    for homepage in range(0, how_many_times_to_check_page, +1):
        productPage.getProductPageContainer(site, product, container)
        loadtime = self.wd.execute_script("return performance.timing.loadEventEnd - performance.timing.navigationStart;")
        arraysList.append(loadtime)
        # Clearing the cache ready for next test - This function no longer works
        clear_cache(self.wd)
    average = sum(arraysList) / len(arraysList)
    #Making the average figure into seconds
    average_in_seconds = (average / 1000.0)
    print ('The average load time after {0} trys is {1} seconds'.format(how_many_times_to_check_page, average_in_seconds))
    #if average > loadtime_threshold:
    if average > loadtime_threshold:
        print ("Average response time of {0} seconds  was > the set threshold which is {1} so this is a fail".format(average_in_seconds, threshold_in_seconds))
        failures = failures+1
    else:
        print(
        "Page load average time after {0} tries was {1} this is an acceptable response time because it was less than the threshold set to {2} seconds".format(how_many_times_to_check_page, average_in_seconds, threshold_in_seconds))
    return failures

def check_if_order_suspended_opc(order_status, suspend, email, suspend_reason, failures):
    if suspend == "notsuspend":
        if order_status == 2:
            print("PASS - Order has not been suspended")
        else:
            failures = failures + 1
            print("FAIL - Order was suspended")
    elif suspend == "suspend":
        if order_status == 1:
            print("PASS - Order has been suspended")
            if email == 'automatedfraud@lovehoney.co.uk':
                if suspend_reason == 39:
                    print("PASS - Blacklisted customer email address order has been given the correct suspend reason")
                    print(order_status)
                else:
                    failures = failures + 1
                    print(suspend_reason)
                    print(
                        "FAIL - Blacklisted customer email address order has not been given the suspend reason we expect")
            elif suspend_reason == 2:
                print("PASS - Order has been given the suspend reason of Security required")
        else:
            failures = failures + 1
            print("FAIL - Order was not suspended")
    return failures



def check_order_payment_text_opc(suspendlineone, cv2_message, failures):
    if suspendlineone == "201":
        if cv2_message == "DATA NOT CHECKED":
            print(
                "PASS - We were testing the Braintree data not checked reason and the payment text is showing : Success (DATA NOT CHECKED)")
        else:
            failures = failures + 1
            print(
                "FAIL - We were testing the Braintree data not checked reason and the payment text is not showing : Success (DATA NOT CHECKED)")

    elif suspendlineone == "200":
        if cv2_message == "NO DATA MATCHES":
            print(
                "PASS - We were testing the Braintree Does not match reason and the payment text is showing : Success (NO DATA MATCHES)")
        else:
            failures = failures + 1
            print(
                "FAIL - We were testing the Braintree Does not match reason and the payment text is not showing : Success (NO DATA MATCHES)")
    return failures

def check_link_numbers(total_links, expectedlinks, tolerance, failures):
    if total_links > expectedlinks + tolerance or total_links < expectedlinks - tolerance:
        failures += 1
        print("Number of links in page header exceeds the range (+ or - 10 from %s) - FAIL" % expectedlinks)
        print("Actual number of links counted on page %s" % total_links)
    else:
        print("Number of links found is in the expected range + - 10 from %s - PASS" % expectedlinks)
        print("Actual number of links counted on page %s" % total_links)
    return failures

# This function checks and updates stock levels for a specified product in a specified warehouse
# The Product IDs is mapped to a Variety Id which is required by the SQL function
def checkAndUpdateStock(site, productId):

    MIN_STOCK_LEVEL = 1000

    if(site == 'AU' or site == 'NZ'):
        warehouseId = 18
    elif(site == 'CM' or site == 'CA'):
        warehouseId = 23
    else:
        warehouseId = 2

    if(productId == '319'):     # BASICS Mini Vibrator
        varietyId = 370
    elif(productId == '7361'):  # Anal Ease
        varietyId = 7361
    elif(productId == '36212'):
        varietyId = 66840
    elif(productId == '26153'):  # Expensive dildo used for Klarna 'pay in 3' option
        varietyId = 40342
    elif(productId == '26153'):
        varietyId = 54811
    elif(productId == '7361'):
        varietyId = 7361
    elif(productId == '31710'): # Used by KL-3 UK Master Checkout
        varietyId = 54811
    elif(productId == '9670'):  # Used by ES Master Checkout
        varietyId = 9670
    elif(productId == '38263'): # Used by PACP check
        varietyId = 70633
    elif(productId == '22112'): # Used by Braintree declined tests
        varietyId = 31239
    elif(productId == '38736'): # Used by Braintree declined tests
        varietyId = 71318
    else:
        print('Error: The checkAndUpdateStock function could not find a product Id')

    stockNum = sql.getStock(warehouseId, varietyId)
    print('Stock Level: {} in {} warehouse'.format(stockNum, site))

    if(int(stockNum) < MIN_STOCK_LEVEL):
        sql.updateStock(warehouseId, varietyId)

def increasingly_on_add_to_basket(wd):
    try:
        #Going to see if Increasling pop's up on live
        wd.find_element_by_xpath('//*[@id="plp_modal"]/div[3]/a[1]').click()
        print("Increasingly shown")
    except:
        print("No increasingly shown moving on")

def test_for_js_errors(self, page, failures):
    # To check for js errors by looking at the console log in chrome
    # To use this you need to set some option in the test runner file:
    # "caps['loggingPrefs'] = {'browser': 'ALL'}" need to be added before the setup def
    wd = self.wd
    wd.get(page)
    print("The current page being tested for console errors is {0}".format(page))
    try:
        failures = failures+get_browser_console_log(self)
        print("---------------------------------------------------------------")
    except:
        print('could not get console log')
        failures+=1
        traceback.print_exc()
    return failures

def get_browser_console_log(self):
    "Get the browser console log"
    print("Checking the chrome console log for errors, if none are found no messages will be logged below....")
    failures = 0
    logfile = self.wd.get_log('browser')
    #Tried to use 'or' rather than have 2 if statements but was not working as expected
    try:
        if "WARNING" in str(logfile):
        #for entry in logfile:
            #if "'level': 'WARNING'" in entry:
            failures = failures+1
            print("Error found in console log see below for log")
            print(logfile)
        elif "SEVERE" in str(logfile):
            failures = failures + 1
            print("Error found in console log see below for log")
            print(logfile)
    except:
        print('could not get console log')
        failures = failures + 1
        traceback.print_exc()
    return failures

def screenshot_on_fail(wd, fileText):
    URL = wd.current_url
    print (URL)
    URL = URL.replace("/", "-")
    URL = URL.replace(":", "-")
    URL = URL.replace(".", "-")
    print (URL)
    fileName = ("TestScreenshots\Fail-%s-%s.png" % (URL, fileText))
    wd.get_screenshot_as_file(fileName)

def open_your_eyes(wd, testname):
    eyes = Eyes()
    eyes.api_key = 'uBJoMlj1105XbvHvUAnpnv2Mlun71085gZdkbgb3zpLKI108s110'
    eyes.force_full_page_screenshot = True
    eyes.match_level = 'STRICT'
    time.sleep(2)
    eyes.open(wd, app_name='Applitools', test_name=testname)
    time.sleep(2)
    return eyes

def close_your_eyes(failures, eyes, testname):
    try:
        # To let eyes get it's house in order...
        time.sleep(2)
        eyes.close()
        # If we get here then the eyes test passed
        print ('The Applitools eyes test {0} was a PASS'.format(testname))
    except Exception as e:
        #If compare falls this will also come into this code block
        print ('fell over eyes, please check applitools as it could be a compare error test name to check is %s' % testname)
        print(e.args)
        failures = failures + 1
    return failures

#Compares and checks text
def check_text(expected, actual, failures):
    print (actual)
    if expected == actual:
        print ('PASSED: Correct Text Shown {0}'.format(expected))
    else:
        print ('FAILED: Incorrect Text Shown {0}'.format(actual))
        failures = failures + 1
    return failures


#Compares and checks text
def check_text_not_strict(expected, actual, failures):
    print (actual)
    if expected in actual:
        print ('PASSED: Correct Text Shown {0}'.format(expected))
    else:
        print ('FAILED: Incorrect Text Shown {0}'.format(actual))
        failures = failures + 1
    return failures

def select_option_from_header_flyouts(wd, flyoutXpath, flyoutitemXpath, failures):
    waitItOut(wd, flyoutXpath, 'XPATH', 'YES', failures)
    waitItOut(wd, flyoutitemXpath, 'XPATH', 'YES', failures)
    return failures

def waitforXpathText(wd, xpath, name, failures):
    try:
        WebDriverWait(wd, 10).until(EC.text_to_be_present_in_element((By.XPATH, xpath), name))
        print ("Successfully waited for {0}".format(name))
    except:
        failures += 1
        print("FAIL - the element you are waiting for never showed up")
    return failures

def add_multiple_items_to_basket(site, targetContainerID, productPage, productIDs):
    print('Trying to add {} products...'.format(len(productIDs)))
    addTotal = 0
    for id in productIDs:
        productPage.getSelectedProductPage(site, id, targetContainerID)
        productPage.accept_gdpr_cookies()
        try:
            productPage.addProductToBasket()
            addTotal = addTotal + 1
        except:
            pass
    print('Added {} products'.format(addTotal))

# This function returns the test run ID in which test results are to be written to
# If nothing is found then the original 'Beta Automated' test run Id is used (7214)
def getTestRailRunId(testRunType):
    runID = "7214"
    try:
        if testRunType == 'beta':
            f = open("../Batches/betatestrunid.cfg", "r")
        elif testRunType == 'beta-secpay':
            f = open("../Batches/secpaytestrunid.cfg", "r")
        else:
            f = open("../Batches/containertestrunid.cfg", "r")
        runID = f.read()
        f.close()
    except:
        print('Could not open the Test Run config file, using run 7214')

    return runID

def getTestRailRunIdReinit(testRunType):
    runID = "7214"
    try:
        if testRunType == 'beta':
            f = open("Batches/betatestrunid.cfg", "r")
        elif testRunType == 'beta-secpay':
            f = open("Batches/secpaytestrunid.cfg", "r")
        else:
            f = open("Batches/containertestrunid.cfg", "r")
        runID = f.read()
        f.close()
    except:
        print('Could not open the Test Run config file, using run 7214')

    return runID