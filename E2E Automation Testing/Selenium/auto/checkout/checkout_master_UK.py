__author__ = 'harrison.davies'
import sys
import time
import unittest
from io import StringIO
from selenium import webdriver
from libraries import misc, testlib
import os


class master_checkoutUK(unittest.TestCase):
    options = webdriver.ChromeOptions()
    #options = webdriver.Firefox(executable_path='C:/geckodriver/geckodriver.exe')
    #options = webdriver.FirefoxProfile()
    options.add_argument('--start-maximized')
    prefs = {"credentials_enable_service": False}
    options.add_experimental_option("prefs", prefs)
    caps = options.to_capabilities()

    def setUp(self):
        self.wd = webdriver.Remote(
            command_executor=os.environ.get(
                'SELENIUM_HUB', 'http://10.7.1.226:4444/wd/hub'),
            desired_capabilities=self.caps
        )
        # self.wd.implicitly_wait(15)


    def test_checkout(self):

        start_time = time.time()
        failures = 0
        # Store the reference, in case you want to show things again in standard output
        old_stdout = sys.stdout
        # This variable will store everything that is sent to the standard output
        result = StringIO()
        sys.stdout = result

        targetContainerID = os.environ.get('TARGET_CONTAINER_ID', 'beta')
        testRunID = misc.getTestRailRunId(targetContainerID)
        print('Test runtype: ' + testRunID)

        failures = failures + testlib.opc_checkout(self, targetContainerID, 'UK', '319', 'LIN', 'CC', 'HAL', '4012000033330026', confirmAddress='YES', checkOpcEmail=0)


        print ("----{0} mins----".format((time.time() - start_time)/60))
        print (failures)
        # Getting all the print statements
        result_string = result.getvalue()

        # Redirect again the std output to screen
        sys.stdout = old_stdout

        print (result_string)
        print (failures)
        misc.sendResultsTR(failures, result_string, '1796', testRunID)
        self.assertTrue(failures==0)

    def tearDown(self):
        self.wd.quit()

if __name__ == '__main__':
    unittest.main()
