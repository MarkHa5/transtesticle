# -*- coding: UTF-8 -*-
# FILE: features/steps/cancel_order_api.py
__author__ = 'gabriel.pichiu'
import json
import requests
from behave import when, then, step
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


@when('the CANCEL ORDER receives a POST request with a body containing OrderId = {OrderId}, Amount = {Billed_amount}, '
      'TransactionId = {TransactionId}')
def step_cancel_order(context, OrderId, Billed_amount, TransactionId):

    url = "https://lovehoneycouk.beta/api/v2/order/cancel"

    payload = {
        "orderId": OrderId,
        "amount": Billed_amount,
        "currency": 'GBP',
        "transactionId": '"' + TransactionId + '"',
        "paymentGatewayCode": 'CC',
        "ticketText": 'order cancelled along with subscription agreement',
        "payment": {
            "token": 12345,
            "customerId": '1234',
            "cardType": 'VISA'
        }
    }

    headers = {
        "Authorization": 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY5MTg2ODcsImlhdCI6MTU3NjY2NTgzMiwiaXNzI'
                         'joibG92ZWhvbmV5LWVjb20tc2l0ZSIsImxhc3RSZXEiOnsiYXV0aG9yaXNlL3Rva2VuIjoxNTQ2OTQ2MjkzLjExNn0sIm'
                         'V4cCI6NDU3NjY2NTgzMiwianRpIjoiRDE0MTE5N0UtNUNEMy00QzEwLUIwMUVGNjk3RkY0Mjk5MDUiLCJhdWQiOiJsb3Z'
                         'laG9uZXktYXBwcyJ9.oUh5GyR8U0Z3UHMlwkMOaAUTur7Z9nRB-4qHoxDhUJqiqbiApGbsXNvAuI_yNGdtbWfklHTKDjK'
                         '6EeSN9gnQgw',
        "Content-Type": 'application/json'
    }

    context.response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False)


@then('the CANCEL ORDER will return HTTP 200 OK status for OrderId = {OrderId}')
def step_cancel_status_code(context, OrderId):
    assert context.response.status_code == 200


@step('the CANCEL ORDER JSON payload contains a Success attribute set to "true" for OrderId = {OrderId}')
def step_cancel_successed(context, OrderId):
    response_data = context.response.json()
    print(response_data['Success'])
    assert response_data['Success'] is True

