# -*- coding: UTF-8 -*-
# FILE: features/steps/suspend_order_api.py
__author__ = 'gabriel.pichiu'
import json
import requests
import urllib3
from behave import when, then, step
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

@when('the SUSPEND ORDER receives a POST request with a body containing OrderId = {OrderId}')
def step_suspend_order(context, OrderId):
    context.request_body = {"OrderId": OrderId}

    url = "https://lovehoneycouk.beta/api/v2/order/suspend"

    payload = {
        "SuspendReason": "Other",
        "TicketText": "Order suspended, contancting braintree so the subscription can be refunded and cancelled",
        "OrderId": OrderId
    }

    headers = {
        "Authorization": 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY5MTg2ODcsImlhdCI6MTU3NjY2NTgzMiwiaXNzI'
                         'joibG92ZWhvbmV5LWVjb20tc2l0ZSIsImxhc3RSZXEiOnsiYXV0aG9yaXNlL3Rva2VuIjoxNTQ2OTQ2MjkzLjExNn0sIm'
                         'V4cCI6NDU3NjY2NTgzMiwianRpIjoiRDE0MTE5N0UtNUNEMy00QzEwLUIwMUVGNjk3RkY0Mjk5MDUiLCJhdWQiOiJsb3Z'
                         'laG9uZXktYXBwcyJ9.oUh5GyR8U0Z3UHMlwkMOaAUTur7Z9nRB-4qHoxDhUJqiqbiApGbsXNvAuI_yNGdtbWfklHTKDjK'
                         '6EeSN9gnQgw',
        "Content-Type": 'application/json'
    }

    context.response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False)


@then('the SUSPEND ORDER will return HTTP 200 OK status for OrderId = {OrderId}')
def step_suspend_status_code(context, OrderId):
    assert context.response.status_code == 200


@step('the SUSPEND ORDER JSON payload contains a Success attribute set to "true" for OrderId = {OrderId}')
def step_suspend_successed(context, OrderId):
    response_data = context.response.json()
    assert response_data['Success'] is True

