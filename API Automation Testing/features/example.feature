# Created by gabriel.pichiu

Feature: ORDER API service
  This feature will be used for testing order api micro service

  Scenario Outline: The SUSPEND ORDER service receives a POST request with a body containing OrderId = <OrderId>
    When the SUSPEND ORDER receives a POST request with a body containing OrderId = <OrderId>
    Then the SUSPEND ORDER will return HTTP 200 OK status for OrderId = <OrderId>
    And the SUSPEND ORDER JSON payload contains a Success attribute set to "true" for OrderId = <OrderId>
    Examples:
      | OrderId  |
      | 19591973 |
      | 19591968 |
      | 19591962 |
      | 19591953 |
      | 19591931 |


  Scenario Outline: The REFUND ORDER receives a POST request with a body containing OrderId = <OrderId>
    When the REFUND ORDER receives a POST request with a body containing OrderId = <OrderId>, Amount = <Billed_amount>, TransactionId = <TransactionId>, PaymentId = <PaymentId>
    Then the REFUND ORDER will return HTTP 200 OK status for OrderId = <OrderId>
    And the REFUND ORDER JSON payload contains a Success attribute set to "true" for OrderId = <OrderId>
    Examples:
      | OrderId  | Billed_amount |TransactionId|PaymentId|
      | 19591973 |     59.49     |fbbr4kv0     |17941129 |
      | 19591968 |     59.49     |d5c22681     |17941107 |
      | 19591962 |     11.18     |25qf3r0p     |17941117 |
      | 19591953 |     11.18     |mcxv4ja5     |17941123 |
      | 19591931 |     11.18     |17jfhpd1     |17941129 |


  Scenario Outline: The CANCEL ORDER receives a POST request with a body containing OrderId = <OrderId>
    When the CANCEL ORDER receives a POST request with a body containing OrderId = <OrderId>, Amount = <Billed_amount>, TransactionId = <TransactionId>
    Then the CANCEL ORDER will return HTTP 200 OK status for OrderId = <OrderId>
    And the CANCEL ORDER JSON payload contains a Success attribute set to "true" for OrderId = <OrderId>
    Examples:
      | OrderId  | Billed_amount |TransactionId|
      | 19590849 |     53.66     |1234567      |
      | 19590850 |     13.98     |1234567      |
      | 19590851 |     62.98     |1234567      |
      | 19591853 |     23.98     |jxva5gzm     |
      | 19591858 |     23.98     |4c3c7phh     |